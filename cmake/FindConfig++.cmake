FIND_PATH(ConfigXX_INCLUDE_DIR
  NAMES libconfig.h++
)

FIND_LIBRARY(ConfigXX_LIBRARY
  NAMES config++
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args( ConfigXX
  FOUND_VAR
    ConfigXX_FOUND
  REQUIRED_VARS
    ConfigXX_LIBRARY
    ConfigXX_INCLUDE_DIR
)

if(ConfigXX_FOUND AND NOT TARGET ConfigXX)
  add_library(ConfigXX UNKNOWN IMPORTED)
  set_property(
    TARGET ConfigXX
    PROPERTY IMPORTED_LOCATION ${ConfigXX_LIBRARY}
  )
  set_property(TARGET ConfigXX
    PROPERTY
      INTERFACE_INCLUDE_DIRECTORIES ${ConfigXX_INCLUDE_DIR}
	)
endif(ConfigXX_FOUND AND NOT TARGET ConfigXX)
