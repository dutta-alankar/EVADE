#pragma once
/** @file */
#include <vector>

namespace EVADE {

namespace Utilities {
class Polynomial;
}

namespace Scaling {
/**
 * ScalingPolicy to turn off all scaling.
 * Shows the minimal interface a ScalingPolicy has to provide.
 */
class Unscaled {
public:
  /**
   * Do nothing.
   */
  void ScaleSystem(std::vector<Utilities::Polynomial> &) {}

  /**
   * Do nothing.
   */
  void UnscaleSolutions(std::vector<std::vector<double>> &) {}
};
} // namespace Scaling
} // namespace EVADE
