#pragma once

/** @file */
#include <vector>

namespace EVADE {

namespace Utilities {
class Polynomial;
}

namespace Scaling {
/**
 * A ScalingPolicy to perform a generically useful variability
 * reduction of the coefficients by rescaling the variables and equations of the
 * system. This should be a useful default in most cases. Physically this allows
 * the solver to reliably find solutions with large field values. This
 * implements the SCLGEN algorithm described in Morgan, which is equivalent to
 * the SCLGNP subroutine from HOMPACK.
 */
class Generic {
public:
  /**
   * Rescales the conditions to minimize coefficient variability.
   * The scaling factors for the variables are stored in scaleV.
   * UnscaleSolutions uses these to reobtain the original scaling for the
   * solutions.
   * @param equations the system to scale
   */
  void ScaleSystem(std::vector<Utilities::Polynomial> &equations);

  /**
   * Unscales the found solutions.
   * @param solutions the scaled solutions to unscale
   */
  void UnscaleSolutions(std::vector<std::vector<double>> &solutions);

private:
  std::vector<double> varscale_ = {};
};
} // namespace Scaling
} // namespace EVADE
