#pragma once

/** @file */

namespace EVADE {
namespace Utilities {

/**
 * This class provides approximate comparisons of floating point values.
 * Implements all relaltional operators with anything convertible to double.
 * e.g.
 * ```
 * Approx(1.) == 1. -> true
 * Approx(1.) == 1.1  -> false
 * Approx(1.).Abs(0.1) == 1.1 -> true
 * Approx(1.).Rel(0.1) == 1.1 -> true
 * Approx(-1.).Rel(1.) < 1. -> true
 * ```
 * The Approx object can be either the first or second object in the comparison.
 */
class Approx {
public:
  /**
   * Constructor from double.
   * @param  value
   */
  explicit Approx(double value);

  /**
   * Sets the absolute tolerance of the Approx object.
   * @param  absTol absolute tolerance
   * @return        reference to the Approx object to allow for chaining:
   * `Approx.Abs(1.).Rel(1e-3)`.
   */
  Approx &Abs(double absTol);

  /**
   * Sets the relative tolerance of the Approx object. It is always applied to
   * the std::max of the compared objects.
   * @param  relTol relative tolerance
   * @return        reference to the Approx object to allow for chaining:
   * `Approx.Rel(1e-3).Abs(1.)`.
   */
  Approx &Rel(double relTol);

  /**
   * Implements lhs < rhs.value && lhs != rhs.
   * Implemented in terms of operator!=.
   */
  friend bool operator<(double lhs, const Approx &rhs);

  /**
   * Implements lhs.value < rhs && lhs != rhs.
   * Implemented in terms of operator!=.
   */
  friend bool operator<(const Approx &lhs, double rhs);

  /**
   * Implements lhs ~ rrhs.
   * Uses the AlmostEqualRelativeAndAbs algorithm from
   * [here](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
   * (with improved handling of infinities).
   */
  friend bool operator==(double lhs, const Approx &rhs);

  /**
   * Trivial overload for swapped arguments.
   */
  inline friend bool operator==(const Approx &lhs, double rhs) {
    return rhs == lhs;
  }

  /**
   * Canonical implementation in terms of operator==.
   */
  inline friend bool operator!=(double lhs, const Approx &rhs) {
    return !(lhs == rhs);
  }

  /**
   * Canonical implementation in terms of operator==.
   */
  inline friend bool operator!=(const Approx &lhs, double rhs) {
    return !(lhs == rhs);
  }

  /**
   * Canonical implementation in terms of operator<.
   */
  inline friend bool operator>(double lhs, const Approx &rhs) {
    return rhs < lhs;
  }

  /**
   * Canonical implementation in terms of operator<.
   */
  inline friend bool operator>(const Approx &lhs, double rhs) {
    return rhs < lhs;
  }

  /**
   * Canonical implementation in terms of operator>.
   */
  inline friend bool operator<=(double lhs, const Approx &rhs) {
    return !(lhs > rhs);
  }

  /**
   * Canonical implementation in terms of operator>.
   */
  inline friend bool operator<=(const Approx &lhs, double rhs) {
    return !(lhs > rhs);
  }

  /**
   * Canonical implementation in terms of operator<.
   */
  inline friend bool operator>=(double lhs, const Approx &rhs) {
    return !(lhs < rhs);
  }

  /**
   * Canonical implementation in terms of operator<.
   */
  inline friend bool operator>=(const Approx &lhs, double rhs) {
    return !(lhs < rhs);
  }

private:
  bool EqualityImplementation(double other) const;

  double relTol_;
  double absTol_;
  double value_;
};

bool operator<(double lhs, const Approx &rhs);
bool operator<(const Approx &lhs, double rhs);
bool operator==(double lhs, const Approx &rhs);

} // namespace Utilities
} // namespace EVADE
