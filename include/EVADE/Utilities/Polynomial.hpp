#pragma once
/** @file */
#include <cstddef>
#include <iosfwd>
#include <map>
#include <string>
#include <vector>

namespace EVADE {
namespace Utilities {
/**
 * Class representing a polynomial.
 * This is simple wrapper for a map, indexed by a vector of unsigned with a
 * double value. The vector specifies the powers of the variables in order and
 * the double is the coefficient for that term. For example
 * `polynomial[{0,1,2}]=2.` corresponds to a term \f$ 2 x_1 x_2^2\f$ in the
 * polynomial.
 */
class Polynomial {
  struct KeyCompare {
    bool operator()(const std::vector<unsigned> &a,
                    const std::vector<unsigned> &b) const;
  };
  std::map<std::vector<unsigned>, double, KeyCompare> data_;

public:
  using value_type =
      decltype(data_)::value_type; ///< Type of a dereferenced iterator.
  using key_type = decltype(data_)::key_type; ///< Type of the keys.
  using iterator = decltype(data_)::iterator; ///< Polynomial iterator.
  using const_iterator =
      decltype(data_)::const_iterator;     ///< Const Polynomial iterator.
  double &operator[](const key_type &key); ///< Accessor with lvalue key.
  double &operator[](key_type &&key);      ///< Accessor with rvalue key.
  double &at(const key_type &key);         ///< Accessor with bounds checking.
  iterator begin() noexcept;               ///< Iterator to first element.
  const_iterator begin() const noexcept;   ///< Const iterator to first element.
  iterator end() noexcept; ///< Iterator to one-past-last element.
  const_iterator end() const
      noexcept;                 ///< Const iterator to one-past-last element.
  size_t size() const noexcept; ///< Number of terms.
  size_t VarCount() const noexcept; ///< Number of variables.

  /**
   * Remove all terms from this Polynomial that vanish if the given variables
   * are zero and reduce the dimensionality of the variable space dropping all
   * zeroed variables.
   * @param  nonZeroVariables lists the indices of all variables that are
   * non-zero
   */
  void SetOtherVariablesToZero(const std::vector<size_t> &nonZeroVariables);

  /**
   * Derivate this Polynomial by the variable var.
   * @param  var index of the variable to derivate by
   * @return the derivative
   */
  Polynomial DerivateBy(size_t var) const;

  /**
   * Creates a string representation of the polynomial.
   * @param  varnames      the names of the variables to use
   * @param  precision     the precision of the floating-point values
   * @param  exponentiator the symbol to denote exponentiation
   * @return               representation of the polynomial
   */
  std::string ToString(const std::vector<std::string> &varnames,
                       int precision = 6,
                       const std::string &exponentiator = "^") const;
};

/** @relates Polynomial
 * Stream operator to print a Polynomial.
 * Calls obj.ToString with generic varnames {x0,x1,...}.
 * @param os  the target stream
 * @param obj the FieldspacePoint to print
 * @return    the target stream
 */
std::ostream &operator<<(std::ostream &os, const Polynomial &obj);

/** @relates Polynomial
 * Calculates the divergence of a polynomial with respect to all non-zero
 * variables.
 * @param  poly             the Polynomial to derivate
 * @param  nonZeroVariables lists the indices of all variables that are non-zero
 * @return                  a vector of derivative Polynomials
 */
std::vector<Polynomial>
DivergenceForNonZero(const Polynomial &poly,
                     const std::vector<size_t> &nonZeroVariables);

/** @relates Polynomial
 * Create a Polynomial describing a unit sphere in d dimensions.
 * 
 * \f$\sum_{i}^{d} x_i^2 - 1\f$
 * 
 * @param d dimensionality
 * @return Polynomial unit sphere polynomial
 */
Polynomial UnitSphere(size_t d);

namespace Detail {
/** @relates EVADE::Utilities::Polynomial
 * Checks whether the given term of a Polynomial is zero provided a list of
 * variables that are zero. Is true if and only if at least one of the zero
 * variables has non-zero power in the term.
 * @param  term      the term to check
 * @param  varIsZero true if the corresponding variable is zero
 * @return           is the term zero
 */
bool TermIsZero(const Polynomial::value_type &term,
                const std::vector<bool> &varIsZero);

/** @relates EVADE::Utilities::Polynomial
 * Takes the derivative of a single term of a polynomial.
 * @param  term the term to derivate
 * @param  var  index of the variable to derivate by
 * @return      the derivative of the term
 */
Polynomial::value_type DerivateTermBy(const Polynomial::value_type &term,
                                      size_t var);
} // namespace Detail

} // namespace Utilities
} // namespace EVADE
