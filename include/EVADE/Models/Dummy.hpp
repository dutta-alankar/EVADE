#pragma once

/** @file */
#include <string>
#include <vector>

#include "EVADE/Utilities/Polynomial.hpp"

namespace EVADE {
namespace Models {
/**
 * Dummy class showcasing the interface a model has to provide.
 */
class Dummy {
public:
  /////////////////////// Static member functions ///////////////////////////

  /**
   * Get the names of the fields for this model.
   * @return vector of fieldnames
   */
  static const std::vector<std::string> &FieldNames();

  /**
   * Get the names of the parameters for this model.
   * @return vector of fieldnames
   */
  static const std::vector<std::string> &ParameterNames();

  /**
   * Get the potential for given parameters.
   * @param  parameters the model parameters
   * @return            the potential as a polynomial
   */
  static Utilities::Polynomial VPoly(const std::vector<double> &parameters);

  /**
   * Get the quartic part of the potential for given parameters.
   * @param  parameters the model parameters
   * @return            the potential as a polynomial
   */
  static Utilities::Polynomial VPoly4(const std::vector<double> &parameters);

  /**
   * Get the value of the potential at a fieldspace point.
   * @param  parameters the model parameters
   * @param  fields     the field values
   * @return            the potential value
   */
  static double V(const std::vector<double> &parameters,
                  const std::vector<double> &fields);

  /**
   * Get the fieldspace location of the initial vacuum for given parameters.
   * @param  parameters the model parameters
   * @return            field values at initial vacuum
   */
  static std::vector<double>
  InitialVacuum(const std::vector<double> &parameters);

  ////////////////////// Non-Static member functions ////////////////////////
private:
  std::vector<double> p_;

public:
  /**
   * Construct a model with the given parameters.
   * @param parameters the model parameters
   */
  Dummy(const std::vector<double> &parameters);

  /**
   * The quadratic coefficient in a given fieldspace direction from the
   * InitialVacuum.
   * @param  direction the fieldspace direction
   * @return           the 2nd order coefficient
   */
  double Msq(const std::vector<double> &direction) const;

  /**
   * The cubic coefficient in a given fieldspace direction from the
   * InitialVacuum.
   * @param  direction the fieldspace direction
   * @return           the 3rd order coefficient
   */
  double A(const std::vector<double> &direction) const;

  /**
   * The quartic coefficient in a given fieldspace direction from the
   * InitialVacuum.
   * @param  direction the fieldspace direction
   * @return           the 4th order coefficient
   */
  double Lam(const std::vector<double> &direction) const;
};
} // namespace Models
} // namespace EVADE
