#pragma once
//! @file
#include "EVADE/Fieldspace/Point.hpp"
#include <vector>

namespace EVADE {

namespace Fieldspace {

namespace IO {
/**
 * Include the origin EVADE::Fieldspace::Point of a
 * EVADE::Fieldspace::Direction when printing.
 */
enum class PrintOrigin { yes, no };
} // namespace IO

//! Represents a direction in Fieldspace by two Points.
class Direction {
public:
  /**
   * @brief Construct a new Direction object from two Point objects.
   * @param origin
   * @param target
   */
  Direction(Point origin, Point target);

  /**
   * @brief Get the origin Point.
   * @return const Point& origin
   */
  const Point &Origin() const;

  /**
   * @brief Get the target Point.
   * @return const Point& target
   */
  const Point &Target() const;

  /**
   * @brief Calculate the normalized direction from origin to target.
   * @return std::vector<double> the direction, vector normalized to 1
   */
  std::vector<double> Dir() const;

  /**
   * @brief Get the distance between origin and target.
   * @return double distance
   */
  double Distance() const;

  //! Absolute tolerance for distance comparison.
  static constexpr double atolDist = 0;
  //! Relative tolerance for distance comparison.
  static constexpr double rtolDist = 1e-5;

  /**
   * @brief Checks for approximate equality of two fieldspace Directions.
   * Check that the distance agrees within Direction::atolDist and
   * Direction::rtolDist and that origin and target are both approximately
   * equal.
   * @param a first Direction
   * @param b second Direction
   * @return approximately equal?
   */
  friend bool operator==(const Direction &a, const Direction &b);

  /**
   * @brief Checks if a is smaller and not approximately equal to b.
   * Sorts first by origin, second by distance and third by target.
   * @param a first Direction
   * @param b second Direction
   * @return a<b?
   */
  friend bool operator<(const Direction &a, const Direction &b);

  /**
   * Converts a Direction into a string.
   * Includes the origin (optional) and target point.
   * @param  printOrigin       Include the origin point.
   * @param  printLabels        If true, formats the output with labels and
   * braces around the field vector, else, simply outputs the values.
   * @param  precision          The floating point precision to use (matches
   * Point::rTolV).
   * @param  separator          The separator betweent the individual values.
   * @return                    A string representation of the FieldspacePoint.
   */
  std::string ToString(IO::PrintOrigin printOrigin = IO::PrintOrigin::no,
                       IO::PrintLabels printLabels = IO::PrintLabels::yes,
                       int precision = 7,
                       const std::string &separator = ", ") const;

private:
  Point origin_;
  Point target_;
  std::vector<double> dir_;
  double distance_;
};

/** @relates Direction
 * Canonical implementation in terms of operator==().
 */
inline bool operator!=(const Direction &a, const Direction &b) {
  return !(a == b);
}

/** @relates Direction
 * Canonical implementation in terms of operator<().
 */
inline bool operator>(const Direction &a, const Direction &b) { return b < a; }

/** @relates Direction
 * Canonical implementation in terms of operator<().
 */
inline bool operator<=(const Direction &a, const Direction &b) {
  return !(b < a);
}

/** @relates Direction
 * Canonical implementation in terms of operator<().
 */
inline bool operator>=(const Direction &a, const Direction &b) {
  return !(a < b);
}

} // namespace Fieldspace
} // namespace EVADE
