#pragma once
/** @file */
#include <iosfwd>
#include <string>
#include <vector>

namespace EVADE {

namespace Fieldspace {

namespace IO {
//! @brief Include the FieldNames when printing a EVADE::Fieldspace::Point
enum class PrintLabels { yes, no };
} // namespace IO

/**
 * @brief A Point in Fieldspace
 */
class Point {
public:
  /**
   * @brief Construct a new Point object
   *
   * @param fieldValues
   * @param potentialValue
   */
  Point(std::vector<double> fieldValues, double potentialValue);

  /**
   * @brief Checks if the Potential at this Point is about as deep as at comp.
   * Uses the Utilities::Approx comparison with tolerances atolV and rtolV;
   *
   * @param comp the Point to compare with
   * @return degenerate?
   */
  bool Degenerate(const Point &comp) const;

  /**
   * @brief Checks if fieldValues of this Point are equivalent to the one at
   * comp. Uses the Utilities::Approx comparison with tolerances
   * Point::atolFields and Point::rtolFields;
   *
   * @param comp the Point to compare with
   * @return degenerate?
   */
  bool EqualFields(const Point &comp) const;

  /**
   * @brief Get the field values at this Point
   *
   * @return const std::vector<double>&  field vector
   */
  const std::vector<double> &Fields() const;

  /**
   * @brief Get the potential value at this Point
   *
   * @return double
   */
  double V() const;

  //! Absolute tolerance for field value comparison.
  static constexpr double atolFields = 0.1;
  //! Relative tolerance for field value comparison.
  static constexpr double rtolFields = 1e-3;
  //! Absolute tolerance for potential value comparison.
  static constexpr double atolV = 10;
  //! Relative tolerance for potential value comparison.
  static constexpr double rtolV = 1e-6;

  /**
   * @brief Checks for approximate equality of two fieldspace Points.
   *
   * Uses Degenerate() and EqualFields().
   *
   * @param a first Point
   * @param b second Point
   * @return approximately equal?
   */
  friend bool operator==(const Point &a, const Point &b);

  /**
   * @brief Checks if a is smaller and not approximately equal to b.
   *
   * Uses Degenerate() and EqualFields(). Sorts first by depth of the potential
   * and second inverse lexicographically by the field values.
   *
   * @param a first Point
   * @param b second Point
   * @return a<b?
   */
  friend bool operator<(const Point &a, const Point &b);

  /**
   * Converts a Point into a string.
   * Includes the potential value and field vector.
   * @param  printLabels        If true, formats the output with labels and
   * braces around the field vector, else, simply outputs the values.
   * @param  precision          The floating point precision to use (default
   * matches Point::rtolV).
   * @param  separator          The separator betweent the individual values.
   * @return                    A string representation of the FieldspacePoint.
   */
  std::string ToString(IO::PrintLabels printLabels = IO::PrintLabels::yes,
                       int precision = 7,
                       const std::string &separator = ", ") const;

private:
  std::vector<double> fields_;
  double V_;
};

/** @relates Point
 * Stream operator to print a Point.
 * Uses the default formatted Point::ToString().
 * @param os  the output stream
 * @param obj the Point to print
 * @return    the output stream
 */
std::ostream &operator<<(std::ostream &os, const Point &obj);

/** @relates Point
 * Canonical implementation in terms of operator==().
 */
inline bool operator!=(const Point &a, const Point &b) { return !(a == b); }

/** @relates Point
 * Canonical implementation in terms of operator<().
 */
inline bool operator>(const Point &a, const Point &b) { return b < a; }

/** @relates Point
 * Canonical implementation in terms of operator<().
 */
inline bool operator<=(const Point &a, const Point &b) { return !(b < a); }

/** @relates Point
 * Canonical implementation in terms of operator<().
 */
inline bool operator>=(const Point &a, const Point &b) { return !(a < b); }

} // namespace Fieldspace
} // namespace EVADE
