#pragma once
/** @file */
#include "EVADE/Fieldspace/Point.hpp"
#include "EVADE/Utilities/Polynomial.hpp"
#include "EVADE/Utilities/Utilities.hpp"
#include <algorithm>
#include <cstddef>
#include <string>
#include <vector>

namespace EVADE {

namespace Scaling {
class Generic;
}
namespace Solver {
class Hom4ps2;
}

/**
 * Template class to solve the stationarity conditions of a model.
 * @tparam Model the current model, only static member functions are used
 * @tparam ScalingPolicy the ScalingPolicy to use, public base class
 * @tparam SolverPolicy the solver to use, public base class
 */
template <class Model, class ScalingPolicy = Scaling::Generic,
          class SolverPolicy = Solver::Hom4ps2>
class StationarityConditions : public ScalingPolicy, public SolverPolicy {
public:
  /**
   * Default constructor.
   * Requires Scaling and Solver Policies to be default-constructible.
   */
  explicit StationarityConditions() : ScalingPolicy{}, SolverPolicy{} {}

  /**
   * Constructor from manually created scaler.
   * Requires ScalingPolicy to be copy-constructible and SolverPolicy to be
   * move-constructible.
   * @param  scaler customized ScalingPolicy object
   */
  explicit StationarityConditions(ScalingPolicy scaler)
      : ScalingPolicy{std::move(scaler)}, SolverPolicy{} {}

  /**
   * Constructor from manually created solver.
   * Requires ScalingPolicy to be default-constructible and SolverPolicy to be
   * move-constructible.
   * @param  solver customized ScalingPolicy object
   */
  explicit StationarityConditions(SolverPolicy solver)
      : ScalingPolicy{}, SolverPolicy{std::move(solver)} {}

  /**
   * Constructor from manually created scaler and solver.
   * Requires ScalingPolicy and SolverPolicy to be move-constructible.
   * @param  scaler customized ScalingPolicy object
   * @param  solver customized ScalingPolicy object
   */
  explicit StationarityConditions(ScalingPolicy scaler, SolverPolicy solver)
      : ScalingPolicy{std::move(scaler)}, SolverPolicy{std::move(solver)} {}

  /**
   * Solve the stationarity conditions for given parameters and a set of fields
   * to consider.
   * @param  parameters    parameter point, passed to the model member functions
   * @param  nonZeroFields names of the fields to consider, passed to
   * Model::StatCondsForFields
   * @return               a vector of FieldspacePoint instances containing the
   * real solutions of the stationarity conditions
   */
  std::vector<Fieldspace::Point>
  SolveForFields(const std::vector<double> &parameters,
                 const std::vector<std::string> &nonZeroFields) {
    const auto positions =
        Utilities::PositionsOfElementsIn(nonZeroFields, Model::FieldNames());

    auto statConds =
        Utilities::DivergenceForNonZero(Model::VPoly(parameters), positions);

    std::vector<std::string> reducedFieldNames;
    for (size_t i : positions) {
      reducedFieldNames.emplace_back(Model::FieldNames()[i]);
    }

    ScalingPolicy::ScaleSystem(statConds);
    auto solutions = SolverPolicy::Call(statConds, reducedFieldNames);
    ScalingPolicy::UnscaleSolutions(solutions);
    for (auto &x : solutions) {
      Utilities::ReinsertZeroFields(x, positions, Model::FieldNames().size());
    }
    std::vector<Fieldspace::Point> result;
    for (auto &x : solutions) {
      double pot = Model::V(parameters,x);
      result.emplace_back(std::move(x), pot);
    }
    return result;
  }

  // std::vector<FieldspacePoint>
  // BfBForFields(const std::vector<double> &parameters,
  //              const std::vector<std::string> &nonZeroFields) {
  //   const auto positions =
  //       Utilities::PositionsOfElementsIn(nonZeroFields, Model::FieldNames());
  //   auto statConds =
  //       Utilities::DivergenceForNonZero(Model::VPoly4(parameters), positions);

  //   auto sizeSort = [](const Utilities::Polynomial &a,
  //                      const Utilities::Polynomial &b) -> bool {
  //     return a.size() < b.size();
  //   };
  //   *std::max_element(statConds.begin(), statConds.end(), sizeSort) =
  //       Utilities::UnitSphere(statConds[0].VarCount());

  //   std::vector<std::string> reducedFieldNames;
  //   for (size_t i : positions) {
  //     reducedFieldNames.emplace_back(Model::FieldNames()[i]);
  //   }

  //   ScalingPolicy::ScaleSystem(statConds);
  //   auto solutions = SolverPolicy::Call(statConds, reducedFieldNames);
  //   ScalingPolicy::UnscaleSolutions(solutions);
  //   for (auto &x : solutions) {
  //     ReinsertZeroFields(x, positions, Model::FieldNames().size());
  //   }

  //   Model mod(parameters);
  //   std::vector<FieldspacePoint> result;
  //   for (const auto &x : solutions) {
  //     result.emplace_back(x, mod.Lam(x));
  //   }
  //   return result;
  // }
};
} // namespace EVADE
