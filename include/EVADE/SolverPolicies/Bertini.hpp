#pragma once
/** @file */

#include "EVADE/Utilities/TempDir.hpp"

#include <string>
#include <vector>

namespace EVADE {

namespace Utilities {
class Polynomial;
}

namespace Solver {

/**
 * SolverPolicy interfacing with the Bertini homotopy solver. Since Bertini is a
 * binary this interfacing has to happen via files and is made thread-safe via
 * the use of a TempDir instance. This type is therefore move-only.
 */
class Bertini {
public:
  /**
   * Solves a system of polynomial equations using Bertini.
   * This function is the required SolverPolicy interface.
   * It takes a system of equations and names for the variables (ordered as they
   * are in the polynomial) and returns a vector of solutions (where the fields
   * are ordered as in varnames).
   * @param equations the system of equations to solve
   * @param varnames  the variable names corresponding to equations
   * @return the solutions of the system ordered as varnames
   */
  std::vector<std::vector<double>>
  Call(const std::vector<Utilities::Polynomial> &equations,
       const std::vector<std::string> &varnames);

  /**
   * Constructor which can be used to change the path where the TempDir
   * workspace will be created or adjust the number of bertini threads.
   * @param  path     path to the directory
   * @param  nThreads number of parallel threads to use in bertini, 1 for serial
   * processing
   */
  explicit Bertini(std::string path = ".", int nThreads = 1);

private:
  void WriteInput(const std::vector<Utilities::Polynomial> &equations,
                  const std::vector<std::string> &varnames);

  int Run();

  std::vector<std::vector<double>>
  ReadResult(const std::vector<std::string> &varnames);

  Utilities::TempDir folder_;
  int nThreads_;
};
} // namespace Solver
} // namespace EVADE
