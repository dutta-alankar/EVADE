import matplotlib.pyplot as plt
import numpy as np

def plotStability(ax, df, x, y, plotsettings):
    from matplotlib.cm import get_cmap
    uncalculated = -1
    initial = -2
    initDegenerate = -3
    notDeeper = -4
    unbounded = -5

    long_lived = df.fast_B > 440
    med_lived = (df.fast_B > 390) & (df.fast_B < 440)
    short_lived = (df.fast_B > 0) & (df.fast_B < 390)
    tachyonic = (df.fast_B == 0)
    stable = (df.fast_B == initial) | (
        df.fast_B == initDegenerate) | (df.fast_B == notDeeper)
    unbounded = df.stat_nUnbounded > 0
    error = (df.fast_B < -3) | (df.fast_B == uncalculated)
    cmap = get_cmap('RdYlGn')

    if np.any(short_lived):
        ax.scatter(df[short_lived][x], df[short_lived][y],
                   c=cmap(0.15), label=r"short-lived", **plotsettings)

    if np.any(med_lived):
        ax.scatter(df[med_lived][x], df[med_lived][y], c=cmap(0.36),
                   label=r"$390<B<440$", **plotsettings)
    if np.any(long_lived):
        ax.scatter(df[long_lived][x], df[long_lived][y],
                   c=cmap(0.75), label=r"long-lived", **plotsettings)
    if np.any(stable):
        ax.scatter(df[stable][x], df[stable][y], c=cmap(0.96),
                   label="stable", **plotsettings)
    if np.any(unbounded):
        ax.scatter(df[unbounded][x], df[unbounded][y], c="C5",
                   label=r"$unbounded$", **plotsettings)
    if np.any(tachyonic):
        ax.scatter(df[tachyonic][x], df[tachyonic][y],
                   c="C4", label="tachyonic", **plotsettings)
    if np.any(error):
        ax.scatter(df[error][x], df[error][y], c="C6",
                   label="ERROR", **plotsettings)

def plotStabilityVevacious(ax, df, x, y, plotsettings):
    from matplotlib.cm import get_cmap
    long_lived = df.VEVRES_0_0 == 0
    short_lived = df.VEVRES_0_0 == -1
    stable = df.VEVRES_0_0 == 1
    cmap = get_cmap('RdYlGn')
    if np.any(short_lived):
        ax.scatter(df[short_lived][x], df[short_lived][y],
                   c=cmap(0.15), label=r"short-lived", **plotsettings)
    if np.any(long_lived):
        ax.scatter(df[long_lived][x], df[long_lived][y],
                   c=cmap(0.75), label=r"long-lived", **plotsettings)
    if np.any(stable):
        ax.scatter(df[stable][x], df[stable][y], c=cmap(0.96),
                   label="stable", **plotsettings)


def plotMinimum(ax, df, x, y, categorize, kind, plotsettings):
    for key, options in categorize(df, kind):
        if(np.any(key)):
            ax.scatter(df[key][x], df[key][y], **options, **plotsettings)


def plotStationaryPoints(ax, df, x, categorize, plotsettings):
    def MostUnstable(group):
        if np.max(group.B) > 0:
            return group[group.B == np.min(group[group.B > 0]['B'])].iloc[0]
        # else:
            # return group[group.B == np.max(group[group.B < 0]['B'])].iloc[0]

    unstables = df.groupby(df.index).apply(MostUnstable)
    for key, options in categorize(df, ''):
        if(np.any(key)):
            ax.scatter(df[key][x], df[key]['V'], **options, **plotsettings)
    ax.plot(unstables[x], unstables['V'], c='k',
            marker='', ls='--', lw=2, label=r'MDM')
    ax.set_yscale('symlog')
    ax.set_ylabel(r'$V\ [\mathrm{GeV}^4]$')
