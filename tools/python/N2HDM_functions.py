import numpy as np

def fromScannerS(input, output):
    import pandas as pd

    df = pd.read_table(input,index_col=False)
    list(df)
    df.rename(columns={
        'p_mH1':'mH1','p_mH2':'mH2','p_mH3':'mH3','p_mA':'mA','p_mHc':'mHc',
        'p_tbeta':'tbeta','p_a1':'a1','p_a2':'a2','p_a3':'a3',
        'p_m12sq':'m12sq','p_vs':'vs',
        'p_L1':'L1', 'p_L2':'L2', 'p_L3':'L3', 'p_L4':'L4', 'p_L5':'L5', 'p_L6':'L6', 'p_L7':'L7', 'p_L8':'L8',
        'p_u11sq':'m11sq', 'p_u22sq':'m22sq', 'p_ussq':'mssq'},inplace=True)

    df['beta'] = np.arctan(df['tbeta'])
    df['vSM'] = 246.21845810181634

    df.to_csv(output,sep='\t')


def index():
    return [
        "beta", "vSM", "vs",
        "L1", "L2", "L3", "L4", "L5", "L6", "L7", "L8",
        "m12sq", "m11sq", "m22sq", "mssq",
        "tbeta", "mH1", "mH2", "mH3", "mA", "mHc", "a1", "a2", "a3"]


def mixingmatrix(a1, a2, a3):
    c1 = np.cos(a1)
    c2 = np.cos(a2)
    c3 = np.cos(a3)
    s1 = np.sin(a1)
    s2 = np.sin(a2)
    s3 = np.sin(a3)

    R = np.array([[c1 * c2, s1 * c2, s2],
                  [-(c1 * s2 * s3 + s1 * c3), c1 * c3 - s1 * s2 * s3, c2 * s3],
                  [-c1 * s2 * c3 + s1 * s3, -(c1 * s3 + s1 * s2 * c3), c2 * c3]])
    assert np.allclose(np.dot(R, np.transpose(R)),
                       np.identity(3)), "Mixing Matrix not unitary!"
    return R


def createpoint(tbeta, vs, m12sq,
                L1=np.nan, L2=np.nan, L3=np.nan, L4=np.nan, L5=np.nan, L6=np.nan, L7=np.nan, L8=np.nan,
                mH1=np.nan, mH2=np.nan, mH3=np.nan, mA=np.nan, mHc=np.nan,
                a1=np.nan, a2=np.nan, a3=np.nan,
                vSM=246.21845810181634):

    beta = np.arctan(tbeta)
    if np.all(np.isfinite([mH1, mH2, mH3, mA, mHc, a1, a2, a3])):
        cb = np.cos(beta)
        sb = np.sin(beta)
        R = mixingmatrix(a1, a2, a3)
        mu = m12sq / sb / cb
        mHi = np.array([mH1, mH2, mH3])
        L1 = 1 / (vSM**2 * cb**2) * (-mu * sb**2 + np.sum(mHi**2 * R[:, 0]**2))
        L2 = 1 / (vSM**2 * sb**2) * (-mu * cb**2 + np.sum(mHi**2 * R[:, 1]**2))
        L3 = 1 / vSM**2 * (-mu + 1 / (sb * cb) *
                           np.sum(mHi**2 * R[:, 0] * R[:, 1]) + 2 * mHc**2)
        L4 = 1 / vSM**2 * (mu + mA**2 - 2 * mHc**2)
        L5 = 1 / vSM**2 * (mu - mA**2)
        L6 = 1 / vs**2 * np.sum(mHi**2 * R[:, 2]**2)
        L7 = 1 / (vs * vSM * cb) * np.sum(mHi**2 * R[:, 0] * R[:, 2])
        L8 = 1 / (vs * vSM * sb) * np.sum(mHi**2 * R[:, 1] * R[:, 2])

    v1 = np.cos(beta) * vSM
    v2 = np.sin(beta) * vSM

    m11sq = m12sq * tbeta - 1 / 2. * \
        (L1 * v1**2 + v2**2 * (L3 + L4 + L5) + vs**2 * L7)
    m22sq = m12sq / tbeta - 1 / 2. * \
        (L2 * v2**2 + v1**2 * (L3 + L4 + L5) + vs**2 * L8)
    mssq = -1 / 2. * (v1**2 * L7 + v2**2 * L8 + vs**2 * L6)
    return [
        beta, vSM, vs,
        L1, L2, L3, L4, L5, L6, L7, L8,
        m12sq, m11sq, m22sq, mssq,
        tbeta, mH1, mH2, mH3, mA, mHc, a1, a2, a3
    ]


def process_df(df):
    uni = df.apply(unitary,axis=1)
    return df[bounded(df)&uni].reset_index(drop=True)


def categorize(df, kind, tol=1e-3):
    import matplotlib.pyplot as plt
    def vev(name):
        return np.abs(df[kind + name]) > tol
    stable = (df.fast_B == -2)|(df.fast_B == -3)|(df.fast_B == -4)
    rd = (vev('vh1r0')) & (vev('vh2r0'))
    s = vev('vhsr0')
    cp = vev('vh2i0')
    cb = vev('vh2rp')
    return [
        [rd & np.invert(s | cp | cb | stable), {
            "label": r"normal $H$ VEVs", 'c': 'C0'}],
        [rd & s & np.invert(cp | cb | stable), {
            "label": r"normal $H, S$ VEVs", 'c': 'C1'}],
        [s & np.invert(cp | cb | rd | stable), {
            "label": r"only $S$ VEVs", 'c': 'C2'}],
        [rd & cp & np.invert(s | cb | stable), {
            "label": r"CP-breaking $H$ VEVs", 'c': 'C3'}],
        [rd & cp & s & np.invert(cb | stable), {
            "label": r"CP-breaking $H, S$ VEVs", 'c': 'C4'}],
        [rd & cb & np.invert(s | cp | stable), {
            "label": r"$e$-breaking $H$ VEVs", 'c': 'C5'}],
        [rd & cb & s & np.invert(cp | stable), {"label": r"$e$-breaking $H, S$ VEVs", 'c': 'C6'}]]


def bounded_cstyle(r):
    if r['L1'] < 0 or r['L2'] < 0 or r['L6'] < 0:
        return False

    if np.sqrt(r['L1'] * r['L6']) + r['L7'] < 0 or np.sqrt(r['L2'] * r['L6']) + r['L8'] < 0:
        return False

    disc12 = np.max([r['L4'] - r['L5'], 0.])

    discab = r['L7'] + r['L8'] * np.sqrt(r['L1'] / r['L2'])

    if discab >= 0 and disc12 + np.sqrt(r['L1'] * r['L2']) + r['L3'] > 0:
        return True

    if discab <= 0 and np.sqrt(r['L2'] * r['L6']) >= r['L8'] and disc12 * r['L6'] + np.sqrt((r['L7']**2 - r['L1'] * r['L6']) * (r['L8']**2 - r['L2'] * r['L6'])) > r['L7'] * r['L8']:
        return True

    return False


def bounded(df):
    D = np.max(np.transpose([df['L4'] - df['L5'], [0.] * len(df['L4'])]),
               axis=1)

    reg1 = ((df['L1'] > 0) &
            (df['L2'] > 0) &
            (df['L6'] > 0) &
            (np.sqrt(df['L1'] * df['L6']) + df['L7'] > 0) &
            (np.sqrt(df['L2'] * df['L6']) + df['L8'] > 0) &
            (np.sqrt(df['L1'] * df['L2']) + df['L3'] + D > 0) &
            (df['L7'] + np.sqrt(df['L1'] / df['L2']) * df['L8'] >= 0))

    reg2 = ((df['L1'] > 0) &
            (df['L2'] > 0) &
            (df['L6'] > 0) &
            (np.abs(df['L8']) <= np.sqrt(df['L2'] * df['L6'])) &
            (np.sqrt(df['L1'] * df['L6']) + df['L7'] > 0) &
            (df['L7'] + np.sqrt(df['L1'] / df['L2']) * df['L8'] <= 0) &
            (np.sqrt((df['L7']**2 - df['L1'] * df['L6']) *
                     (df['L8']**2 - df['L2'] * df['L6'])) > df['L7'] * df['L8'] - (D + df['L3']) * df['L6']))

    return reg1 | reg2


def unitary(r):
    if np.abs(r['L3'] - r['L4']) > 8 * np.pi:
        return False
    if np.abs(r['L3'] + 2 * r['L4'] + 3 * r['L5']) > 8 * np.pi:
        return False
    if np.abs(r['L3'] + 2 * r['L4'] - 3 * r['L5']) > 8 * np.pi:
        return False
    if np.abs(1 / 2. * (r['L1'] + r['L2'] + np.sqrt((r['L1'] - r['L2'])**2 + 4 * r['L4']**2))) > 8 * np.pi:
        return False
    if np.abs(1 / 2. * (r['L1'] + r['L2'] + np.sqrt((r['L1'] - r['L2'])**2 + 4 * r['L5']**2))) > 8 * np.pi:
        return False

    coeffs = [1,
              -6 * r['L1'] - 6 * r['L2'] - 3 * r['L6'],
              36 * r['L1'] * r['L2'] - 16 * r['L3']**2 - 16 * r['L3'] * r['L4'] - 4 * r['L4']**2 +
              18 * r['L1'] * r['L6'] + 18 * r['L2'] *
              r['L6'] - 4 * r['L7']**2 - 4 * r['L8']**2,
              -108 * r['L1'] * r['L2'] * r['L6'] + 48 * r['L3']**2 * r['L6'] + 48 * r['L3'] * r['L4'] * r['L6'] + 12 * r['L4']**2 * r['L6'] + 24 * r['L2'] * r['L7']**2 - 32 * r['L3'] * r['L7'] * r['L8'] - 16 * r['L4'] * r['L7'] * r['L8'] + 24 * r['L1'] * r['L8']**2]

    roots = np.roots(coeffs)
    rroots = roots.real[np.abs(roots.imag) < 1e-5]
    if(np.max(np.abs(rroots)) > 8 * np.pi):
        return False

    return True
