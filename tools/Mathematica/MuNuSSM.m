name = "MuNuSSM";

su2prod[x_List, y_List] := x[[1]]*y[[2]] - x[[2]] y[[1]]

W = yv1 su2prod[L[1], Hu] Conjugate[vr] + 
    yv2 su2prod[L[2], Hu] Conjugate[vr] + 
    yv3 su2prod[L[3], Hu] Conjugate[vr] - 
    lam su2prod[Hd, Hu] Conjugate[vr] +
    (1/3) kap Conjugate[vr] Conjugate[vr] Conjugate[vr] /. {Hu -> {Hup, Hu0}, 
      Hd -> {Hd0, Hdm}, L[1] -> {vl[1], ll[1]}, L[2] -> {vl[2], ll[2]}, 
      L[3] -> {vl[3], ll[3]}};

F = Expand[Total[Flatten[Table[D[W, x] Conjugate[D[W, x]],
  {x, {Hu0, Hd0, vl[1], vl[2], vl[3], ll[1], ll[2], ll[3],
    Conjugate[vr]}}]]] /. Hup -> 0];

D2 = Total[Flatten[Table[(2 (ConjugateTranspose[x1].x2)
  (ConjugateTranspose[x2].x1) - (ConjugateTranspose[x1].x1)
    (ConjugateTranspose[x2].x2)),
  {x1, {Hu, Hd, L[1], L[2], L[3]}},
  {x2, {Hu, Hd, L[1], L[2], L[3]}}] /. {Hu -> {{Hup}, {Hu0}},
    Hd -> {{Hd0}, {Hdm}}, L[1] -> {{vl[1]}, {ll[1]}},
    L[2] -> {{vl[2]}, {ll[2]}}, L[3] -> {{vl[3]}, {ll[3]}}}]] /. {Hup -> 0, Hdm -> 0, ll[___] -> 0};

Vsoft = (mHusq Abs[Hu0]^2 + mHdsq Abs[Hd0]^2 + 
      Sum[mLsq[i, j] Cnj[vl[i]] vl[j], {i, 3}, {j, 3}] + 
      mvsq Abs[vr]^2 +
      1/2*(Sum[mLHdsq[i] Cnj[Hd0] vl[i], {i, 3}] + Cnj[Sum[mLHdsq[i] Cnj[Hd0] vl[i], {i, 3}]]) + 
      Sum[Cnj[vr] Tv[i] Hu0 vl[i], {i, 3}] +
      Cnj[Sum[Cnj[vr] Tv[i] Hu0 vl[i], {i, 3}]] - 
      Tlam Cnj[vr] Hd0 Hu0 - 
      Cnj[Tlam Cnj[vr] Hd0 Hu0] +
      (1/3) Tkap Cnj[vr] Cnj[vr] Cnj[vr] +
      Cnj[(1/3) Tkap Cnj[vr] Cnj[vr] Cnj[vr]]) /. mLsq[vars__] /; Greater[vars] :> mLsq @@ Reverse[List[vars]] /. Cnj -> Conjugate //. Conjugate[a_*b_] :> Conjugate[a] Conjugate[b];

V0 = (F + g1^2/8 (Abs[Hu0]^2 - Abs[Hd0]^2 - Sum[Abs[vl[i]]^2, {i,1,3}])^2 +
  g2^2/8 D2 + Vsoft);

norm = Sqrt[2];

parnamerules = {mLsq[i_,j_] :> ToExpression["mLsq"<>ToString[i]<>ToString[j]],
                mLHdsq[i_] :> ToExpression["mLHdsq"<>ToString[i]],
                Tv[i_] :> ToExpression["Tv"<>ToString[i]]}
symFixing = {vhui0 -> 0};

softmassrules = {mHdsq -> ((g1^2*((-1 + tbeta^2)*v0^2 - 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) + 
    (g2^2*((-1 + tbeta^2)*v0^2 - 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) + 
    4*((-2*Sqrt[1 + tbeta^2]*(mLHdsq1*vvlr1EW + mLHdsq2*vvlr2EW + mLHdsq3*vvlr3EW))/Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + 
      Sqrt[2]*tbeta*Tlam*vvrr1EW + kap*lam*tbeta*vvrr1EW^2 + lam^2*((tbeta^2*(-v0^2 + vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2))/
         (1 + tbeta^2) - vvrr1EW^2) + (lam*(vvrr1EW^2 + tbeta^2*(v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2 + vvrr1EW^2))*
        (vvlr1EW*yv1 + vvlr2EW*yv2 + vvlr3EW*yv3))/(Sqrt[1 + tbeta^2]*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2])))/8, 
 mHusq -> ((g1^2*(-((-1 + tbeta^2)*v0^2) + 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) + 
    (g2^2*(-((-1 + tbeta^2)*v0^2) + 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) + (4*kap*lam*vvrr1EW^2)/tbeta + 
    (4*lam^2*(-v0^2 + vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2 - (1 + tbeta^2)*vvrr1EW^2))/(1 + tbeta^2) + 
    (8*lam*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*(vvlr1EW*yv1 + vvlr2EW*yv2 + vvlr3EW*yv3))/Sqrt[1 + tbeta^2] - 
    (4*(-(Sqrt[2]*Tlam*vvrr1EW) + (Sqrt[2]*Sqrt[1 + tbeta^2]*Tv1*vvlr1EW*vvrr1EW)/Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + 
       (Sqrt[2]*Sqrt[1 + tbeta^2]*Tv2*vvlr2EW*vvrr1EW)/Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + 
       (Sqrt[2]*Sqrt[1 + tbeta^2]*Tv3*vvlr3EW*vvrr1EW)/Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + 
       (kap*Sqrt[1 + tbeta^2]*vvlr1EW*vvrr1EW^2*yv1)/Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + tbeta*vvlr1EW^2*yv1^2 + 
       tbeta*vvrr1EW^2*yv1^2 + (kap*Sqrt[1 + tbeta^2]*vvlr2EW*vvrr1EW^2*yv2)/Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + 
       2*tbeta*vvlr1EW*vvlr2EW*yv1*yv2 + tbeta*vvlr2EW^2*yv2^2 + tbeta*vvrr1EW^2*yv2^2 + (kap*Sqrt[1 + tbeta^2]*vvlr3EW*vvrr1EW^2*yv3)/
        Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + 2*tbeta*vvlr1EW*vvlr3EW*yv1*yv3 + 2*tbeta*vvlr2EW*vvlr3EW*yv2*yv3 + 
       tbeta*vvlr3EW^2*yv3^2 + tbeta*vvrr1EW^2*yv3^2))/tbeta)/8, 
 mLsq11 -> ((g1^2*((-1 + tbeta^2)*v0^2 - 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) + 
    (g2^2*((-1 + tbeta^2)*v0^2 - 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) - 
    (4*kap*tbeta*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*vvrr1EW^2*yv1)/(Sqrt[1 + tbeta^2]*vvlr1EW) - 
    (4*(2*mLsq12*vvlr2EW + 2*mLsq13*vvlr3EW + (lam*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*
         (tbeta^2*vvlr3EW^2 - (1 + tbeta^2)*vvrr1EW^2)*yv1)/(1 + tbeta^2)^(3/2) + 
       (Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*(2*mLHdsq1*(1 + tbeta^2) + tbeta*(Sqrt[2]*(1 + tbeta^2)*Tv1*vvrr1EW + 
            lam*tbeta*(-v0^2 + vvlr1EW^2 + vvlr2EW^2)*yv1)))/(1 + tbeta^2)^(3/2) + 
       ((vvrr1EW^2 + tbeta^2*(v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2 + vvrr1EW^2))*yv1*(vvlr1EW*yv1 + vvlr2EW*yv2 + vvlr3EW*yv3))/
        (1 + tbeta^2)))/vvlr1EW)/8, 
 mLsq22 -> ((g1^2*((-1 + tbeta^2)*v0^2 - 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) + 
    (g2^2*((-1 + tbeta^2)*v0^2 - 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) - 
    (4*kap*tbeta*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*vvrr1EW^2*yv2)/(Sqrt[1 + tbeta^2]*vvlr2EW) - 
    (4*(2*mLsq12*vvlr1EW + 2*mLsq23*vvlr3EW + (lam*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*
         (tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2) - (1 + tbeta^2)*vvrr1EW^2)*yv2)/(1 + tbeta^2)^(3/2) + 
       (Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*(2*mLHdsq2*(1 + tbeta^2) + tbeta*(Sqrt[2]*(1 + tbeta^2)*Tv2*vvrr1EW - 
            lam*tbeta*v0^2*yv2)))/(1 + tbeta^2)^(3/2) + ((vvrr1EW^2 + tbeta^2*(v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2 + vvrr1EW^2))*
         yv2*(vvlr1EW*yv1 + vvlr2EW*yv2 + vvlr3EW*yv3))/(1 + tbeta^2)))/vvlr2EW)/8, 
 mLsq33 -> ((g1^2*((-1 + tbeta^2)*v0^2 - 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) + 
    (g2^2*((-1 + tbeta^2)*v0^2 - 2*tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2)))/(1 + tbeta^2) - 
    (4*kap*tbeta*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*vvrr1EW^2*yv3)/(Sqrt[1 + tbeta^2]*vvlr3EW) - 
    (4*(2*mLsq13*vvlr1EW + 2*mLsq23*vvlr2EW + (lam*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*
         (tbeta^2*(vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2) - (1 + tbeta^2)*vvrr1EW^2)*yv3)/(1 + tbeta^2)^(3/2) + 
       ((vvrr1EW^2 + tbeta^2*(v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2 + vvrr1EW^2))*yv3*(vvlr1EW*yv1 + vvlr2EW*yv2 + vvlr3EW*yv3))/
        (1 + tbeta^2) + (Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*(2*mLHdsq3*(1 + tbeta^2) + 
          tbeta*(Sqrt[2]*(1 + tbeta^2)*Tv3*vvrr1EW - lam*tbeta*v0^2*yv3)))/(1 + tbeta^2)^(3/2)))/vvlr3EW)/8, 
 mvsq -> -(Sqrt[2]*tbeta*Sqrt[1 + tbeta^2]*(Tv1*vvlr1EW + Tv2*vvlr2EW + Tv3*vvlr3EW)*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + 
     Sqrt[2]*tbeta*Tlam*(-v0^2 + vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2) + Sqrt[2]*(1 + tbeta^2)*Tkap*vvrr1EW^2 + 
     2*kap^2*(1 + tbeta^2)*vvrr1EW^3 - 2*kap*tbeta*vvrr1EW*(lam*(v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2) - 
       Sqrt[1 + tbeta^2]*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*(vvlr1EW*yv1 + vvlr2EW*yv2 + vvlr3EW*yv3)) + 
     vvrr1EW*(lam^2*(v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2) - 2*lam*Sqrt[1 + tbeta^2]*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2]*
        (vvlr1EW*yv1 + vvlr2EW*yv2 + vvlr3EW*yv3) + (vvlr1EW*yv1 + vvlr2EW*yv2 + vvlr3EW*yv3)^2) - 
     tbeta^2*vvrr1EW*(lam^2*(-v0^2 + vvlr1EW^2 + vvlr2EW^2 + vvlr3EW^2) + vvlr2EW^2*yv1^2 + vvlr3EW^2*yv1^2 - 
       2*vvlr1EW*vvlr2EW*yv1*yv2 + vvlr1EW^2*yv2^2 + vvlr3EW^2*yv2^2 - 2*vvlr3EW*(vvlr1EW*yv1 + vvlr2EW*yv2)*yv3 + 
       (vvlr1EW^2 + vvlr2EW^2)*yv3^2 - v0^2*(yv1^2 + yv2^2 + yv3^2)))/(2*(1 + tbeta^2)*vvrr1EW)}

V = Expand[ComplexExpand[
   V0 /. {Hu0 -> hu0/norm, Hd0 -> hd0/norm, 
      vl[a_] :> nul[a]/norm, vr -> nur/norm} /. {hu0 -> 
      vhur0 + I vhui0, hd0 -> vhdr0 + I vhdi0, 
      nul[1] -> vvlr1 + I vvli1, nul[2] -> vvlr2 + I vvli2, nul[3] -> vvlr3 + I vvli3, 
      nur -> vvrr1 + I vvri1} /. parnamerules /. symFixing]/.softmassrules];

ewvacuum = {
  vhur0 -> Sin[ArcTan[tbeta]]*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + vhur0, 
  vhdr0 ->  Cos[ArcTan[tbeta]]*Sqrt[v0^2 - vvlr1EW^2 - vvlr2EW^2 - vvlr3EW^2] + vhdr0,
  vvlr1 -> vvlr1 + vvlr1EW, vvlr2 -> vvlr2 + vvlr2EW, vvlr3 -> vvlr3 + vvlr3EW,
  vvrr1 -> vvrr1 + vvrr1EW};

fields = {vhur0, vhdr0, vhdi0, 
          vvlr1, vvlr2, vvlr3, 
          vvli1, vvli2, vvli3,
          vvrr1, vvri1};

parameters = {yv1, yv2, yv3, lam, kap,
              g1, g2,
              mLsq12, mLsq13, mLsq23,
              mLHdsq1, mLHdsq2, mLHdsq3,
              Tv1, Tv2, Tv3,
              Tlam, Tkap,
              tbeta, v0, 
              vvlr1EW, vvlr2EW, vvlr3EW, 
              vvrr1EW
              };

generatemunuSSM[] := generateModel[name, V, fields, parameters, ewvacuum]
