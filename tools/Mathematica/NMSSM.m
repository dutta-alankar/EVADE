name = "NMSSM";

su2prod[x_List, y_List] := x[[1]]*y[[2]] - x[[2]] y[[1]]
W = lam S su2prod[Hu, Hd] + kap S^3/3 +
    yt su2prod[Q, Hu] Conjugate[tr] +
    yb su2prod[Hd, Q] Conjugate[br] +
    yl su2prod[Hd, L] Conjugate[lr] /. {Hu -> {Hup, Hu0},
    Hd -> {Hd0, Hdm}, Q -> {tl, bl}, L -> {vl, ll}};
F = Total[
   Flatten[Table[
     D[W, x] Conjugate[D[W, x]], {x, {Hu0, Hd0, Hup, Hdm, S, tl, bl,
       vl, ll, Conjugate[tr], Conjugate[br], Conjugate[lr]}}]]];
D2 = Total[
   Flatten[Table[(2 (ConjugateTranspose[x1].x2) (ConjugateTranspose[
            x2].x1) - (ConjugateTranspose[x1].x1) (ConjugateTranspose[
            x2].x2)), {x1, {Hu, Hd, Q, L}}, {x2, {Hu, Hd, Q,
        L}}] /. {Hu -> {{Hup}, {Hu0}}, Hd -> {{Hd0}, {Hdm}},
      Q -> {{tl}, {bl}}, L -> {{vl}, {ll}}}]];
V0 = (F
     + g1^2/
       8 (Abs[Hu0]^2 + Abs[Hup]^2 - Abs[Hd0]^2 - Abs[Hdm]^2 +
         1/3 Abs[bl]^2 + 2/3 Abs[br]^2 + 1/3 Abs[tl]^2 -
         4/3 Abs[tr]^2 - Abs[ll]^2 + 2 Abs[lr]^2 - Abs[vl]^2)^2
     + g2^2/8 D2
     + g3^2/6 (Abs[tl]^2 - Abs[tr]^2 + Abs[bl]^2 - Abs[br]^2)^2
     + mHusq (Abs[Hu0]^2 + Abs[Hup]^2)
     + mHdsq (Abs[Hd0]^2 + Abs[Hdm]^2)
     + mHssq Abs[S]^2
     + mL3^2 (Abs[ll]^2 + Abs[vl]^2)
     + mE3^2 Abs[lr]^2
     + mQ3^2 (Abs[tl]^2 + Abs[bl]^2)
     + mTOP^2 Abs[tr]^2 + mBOT^2 Abs[br]^2
     + lam Alam su2prod[Hu, Hd] S +
     Conjugate[lam Alam su2prod[Hu, Hd] S ]
     + 1/3 kap Akap S^3 + Conjugate[1/3 kap Akap S^3 ]
     + Conjugate[tr] Tt (Hu0 tl - Hup bl) +
     Conjugate[Conjugate[tr] Tt (Hu0 tl - Hup bl)]
     + Conjugate[br] Tb (Hd0 bl - Hdm tl) +
     Conjugate[Conjugate[br] Tb (Hd0 bl - Hdm tl)]
     + Conjugate[lr] Tl (Hd0 ll - Hdm vl) +
     Conjugate[Conjugate[lr] Tl (Hd0 ll - Hdm vl)]
    ) /. {Hu -> {Hup, Hu0}, Hd -> {Hd0, Hdm}};
norm = Sqrt[2];
slhapars = {v0 -> SLHA::HMIX[3], beta -> SLHA::HMIX[10],
   g1 -> SLHA::GAUGE[1], g2 -> SLHA::GAUGE[2], g3 -> SLHA::GAUGE[3],
   mHusq -> SLHA::MSOFT[22], mHdsq -> SLHA::MSOFT[21],
   mHssq -> SLHA::NMSSMRUN[10], yt -> SLHA::YU[3, 3],
   yb -> SLHA::YD[3, 3], yl -> SLHA::YE[3, 3],
   mQ3^2 -> SLHA::MSQ2[3, 3], mTOP^2 -> SLHA::MSU2[3, 3],
   mBOT^2 -> SLHA::MSD2[3, 3], mL3^2 -> SLHA::MSL2[3, 3],
   mE3^2 -> SLHA::MSE2[3, 3], Tt -> SLHA::TU[3, 3],
   Tb -> SLHA::TD[3, 3], Tl -> SLHA::TE[3, 3],
   lam -> SLHA::NMSSMRUN[1], kap -> SLHA::NMSSMRUN[2],
   Alam -> SLHA::NMSSMRUN[3], Akap -> SLHA::NMSSMRUN[4],
   mueff -> SLHA::NMSSMRUN[5]};
ewvacuum = {vhur0 -> Sin[beta]*v0 + vhur0,
   vhdr0 -> Cos[beta]*v0 + vhdr0, vhsr0 -> norm*mueff/lam + vhsr0} /. slhapars;
symFixing = {vhui0 -> 0, vhurp -> 0, vhuip -> 0, vhdim -> 0};
V = ComplexExpand[
   V0 /. slhapars /. {Hup -> hup/norm, Hdm -> hdm/norm,
      Hu0 -> hu0/norm, Hd0 -> hd0/norm, S -> s/norm, bl -> bl/norm,
      br -> br/norm, tl -> tl/norm, tr -> tr/norm, ll -> ll/norm,
      lr -> lr/norm, vl -> vl/norm} /. {hup -> vhurp + I vhuip,
     hdm -> vhdrm + I vhdim, hu0 -> vhur0 + I vhui0,
     hd0 -> vhdr0 + I vhdi0, s -> vhsr0 + I*vhsi0,
     tl -> vulr3 + I vuli3, tr -> vurr3 + I vuri3,
     bl -> vdlr3 + I vdli3, br -> vdrr3 + I vdri3,
     ll -> velr3 + I veli3, lr -> verr3 + I veri3,
     vl -> vvlr3 + I vvli3}/.symFixing];
fields = {vhur0, vhdr0, vhdi0, vhsr0, vhsi0, vhdrm,
  vulr3, vuli3, vurr3, vuri3, vdlr3, vdli3, vdrr3,
  vdri3, velr3, veli3, verr3, veri3, vvlr3, vvli3};
ewminconds = 
 Simplify[Solve[# == 0 & /@ 
     DeleteCases[
      Simplify[D[V, {fields, 1}] /. ewvacuum /. (# -> 0 & /@ fields)],
       0], {SLHA::MSOFT[22], SLHA::MSOFT[21], 
     SLHA::NMSSMRUN[10]}][[1]]]
V = Expand[V/.ewminconds]
parameters = {SLHA::GAUGE[1], SLHA::GAUGE[2], SLHA::GAUGE[3],
  SLHA::NMSSMRUN[1], SLHA::NMSSMRUN[2], SLHA::NMSSMRUN[3],
  SLHA::NMSSMRUN[4], SLHA::NMSSMRUN[5], SLHA::HMIX[3],
  SLHA::HMIX[10], SLHA::YU[3, 3], SLHA::YD[3, 3], SLHA::YE[3, 3],
  SLHA::MSQ2[3, 3], SLHA::MSU2[3, 3], SLHA::MSD2[3, 3],
  SLHA::MSL2[3, 3], SLHA::MSE2[3, 3], SLHA::TU[3, 3], SLHA::TD[3, 3],
   SLHA::TE[3, 3]};

generateNMSSM[]:= generateModel[name,V,fields,parameters,ewvacuum,parnameRules->SlhaVarnameRule]
