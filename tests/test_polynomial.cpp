#include "EVADE/Utilities/Polynomial.hpp"
#include "catch.hpp"

using EVADE::Utilities::Polynomial;

TEST_CASE("Setting Polynomial variables to zero", "[unit][polynomial]") {
  Polynomial test;

  test[{2, 0, 1}] = 1.;

  SECTION("TermIsZero condition") {
    using EVADE::Utilities::Detail::TermIsZero;
    REQUIRE(TermIsZero(*test.begin(), {true, false, false}));
    REQUIRE_FALSE(TermIsZero(*test.begin(), {false, true, false}));
    REQUIRE(TermIsZero(*test.begin(), {false, false, true}));
  }

  test[{1, 1, 1}] = -2.;
  test[{0, 2, 0}] = -1.;
  test[{0, 0, 1}] = 4.;
  SECTION("Setting term to zero") {
    Polynomial test2{test};

    test2.SetOtherVariablesToZero({1, 2});
    REQUIRE(test2.size() == 2);
    REQUIRE(test2.at({2, 0}) == CatchApprox(-1.));
    REQUIRE(test2.at({0, 1}) == CatchApprox(4.));

    test2 = test;
    test2.SetOtherVariablesToZero({1});
    REQUIRE(test2.size() == 1);
    REQUIRE(test2.at({2}) == CatchApprox(-1.));

    test2 = test;
    test2.SetOtherVariablesToZero({});
    REQUIRE(test2.size() == 0);
  }
}

TEST_CASE("Polynomial term ordering", "[unit][polynomial]") {
  Polynomial test;

  test[{1, 1, 1}] = -1.;
  test[{2, 3, 1}] = -1.;
  test[{0, 2, 0}] = 1.;

  SECTION("First element has lowest total degree") {
    REQUIRE((*test.begin()).second == CatchApprox(1.));
  }

  test[{0, 0, 1}] = 2.;
  SECTION("New lowest order element inserted") {
    REQUIRE((*test.begin()).second == CatchApprox(2.));
  }

  test[{0, 1, 0}] = 3.;
  SECTION("Lexicographically ordered if equal degree") {
    REQUIRE((*test.begin()).second == CatchApprox(2.));
  }
}

TEST_CASE("Polynomial derivatives", "[unit][polynomial]") {
  Polynomial test;

  test[{2, 0, 1}] = 1.;
  test[{1, 0, 1}] = -2.;
  test[{0, 2, 0}] = -1.;
  test[{0, 0, 1}] = 4.;
  test[{0, 0, 2}] = 4.;

  SECTION("Derive by x0, two terms drop") {
    auto derivative = test.DerivateBy(0);

    REQUIRE(derivative.size() == 2);
    REQUIRE(derivative.at({1, 0, 1}) == CatchApprox(2.));
    REQUIRE(derivative.at({0, 0, 1}) == CatchApprox(-2.));
  }

  SECTION("Divergence and set remaining fields to zero") {
    auto divConds = DivergenceForNonZero(test, {1, 2});

    REQUIRE(divConds.size() == 2);
    REQUIRE(divConds[0].size() == 1);
    REQUIRE(divConds[1].size() == 2);
  }
}

TEST_CASE("Polynomial term insertions", "[unit][polynomial]") {
  Polynomial pot;

  pot[{4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 32.;
  pot[{2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 16.;
  pot[{2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 16.;
  pot[{2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 16.;
  pot[{2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 16.;
  pot[{2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 16.;
  pot[{2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 16.;
  pot[{2, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 16.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0}] = 1 / 48.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0}] = 1 / 48.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0}] = 1 / 12.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0}] = 1 / 12.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0}] = 1 / 48.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0}] = 1 / 48.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0}] = 1 / 24.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2}] = 1 / 24.;
  pot[{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 2.;
  pot[{1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 4.;
  pot[{1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}] = 1 / 4.;
  pot[{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}] = 1.;

  REQUIRE(pot.size() == 20);

  SECTION("Throw on incorrect size") { REQUIRE_THROWS(pot[{1}] = 0); }
}
