#include "EVADE/BouncePolicies/Quartic.hpp"
#include "EVADE/Models/Dummy.hpp"
#include "catch.hpp"

using namespace EVADE;

TEST_CASE("Quartic bounce on ModelDummy", "[unit][bounce][quarticbounce]") {
  Bounce::Quartic<Models::Dummy> test{{}};
  REQUIRE(test({}) == CatchApprox(990.884));
}
