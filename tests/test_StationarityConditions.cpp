#include "EVADE/Models/Dummy.hpp"
#include "EVADE/Models/MSSM.hpp"
#include "EVADE/ScalingPolicies/Generic.hpp"
#include "EVADE/ScalingPolicies/Unscaled.hpp"
#include "EVADE/SolverPolicies/Hom4ps2.hpp"
#include "EVADE/StationarityConditions.hpp"
// #include "EVADE/config.h"
#include "catch.hpp"

#include <algorithm>
#include <cmath>

using namespace EVADE;

TEST_CASE("Dummy no scaling", "[unit][statconds][hom4ps2]") {
  StationarityConditions<Models::Dummy, Scaling::Unscaled, Solver::Hom4ps2>
      statconds;

  auto result = statconds.SolveForFields({}, {"x0", "x1"});

  SECTION("checking contained fieldspace points") {
    REQUIRE(result.size() == 2);
    INFO(result[0]);
    INFO(result[1]);
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({-2. / 3., -2. / 3.}, -1000.)) !=
            result.end());
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({0., 0.}, -1000.)) != result.end());
  }
}

TEST_CASE("Dummy generic scaling", "[unit][statconds][hom4ps2][gscaling]") {
  StationarityConditions<Models::Dummy, Scaling::Generic, Solver::Hom4ps2>
      statconds;

  auto result = statconds.SolveForFields({}, {"x0", "x1"});

  SECTION("checking contained fieldspace points") {
    REQUIRE(result.size() == 2);
    INFO(result[0]);
    INFO(result[1]);
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({-2. / 3., -2. / 3.}, -1000.)) !=
            result.end());
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({0., 0.}, -1000.)) != result.end());
  }
}

TEST_CASE("MSSM statconds no scaling", "[unit][statconds][hom4ps2][mssm]") {
  StationarityConditions<Models::MSSM, Scaling::Unscaled, Solver::Hom4ps2>
      statconds;
  std::vector<double> testpars{
      0.36151919899999996, // GAUGE_1
      0.63675145,          // GAUGE_2
      1.04060771,          // GAUGE_3
      -7155.0,             // HMIX_1
      246.21845810181637,  // HMIX_3
      863041.0,            // HMIX_4
      1.5232132235179132,  // HMIX_10
      0.8309101757104086,  // YU_3_3
      0.2319447148681335,  // YD_3_3
      0,                   // YE_3_3
      -50328892.06354008,  // MSOFT_21
      -51196116.93645992,  // MSOFT_22
      8743849.0,           // MSQ2_3_3
      8743849.0,           // MSU2_3_3
      8743849.0,           // MSD2_3_3
      8743849.0,           // MSL2_3_3
      8743849.0,           // MSE2_3_3
      3438.306307089671,   // TU_3_3
      1349.148564788819,   // TD_3_3
      0.,                  // TE_3_3
  };

  SECTION("Minimal field content") {
    auto result = statconds.SolveForFields(testpars, {"vhur0", "vhdr0"});
    size_t nsol = result.size();

    REQUIRE(nsol == 3);
    REQUIRE(std::find(
                result.begin(), result.end(),
                Fieldspace::Point({245.94, 0, 11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                                   0,      0, 0,       0, 0, 0, 0, 0, 0, 0, 0},
                                  -6.10209e7)) != result.end());
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({-245.94, 0, -11.7114, 0, 0, 0, 0, 0,
                                         0,       0, 0,        0, 0, 0, 0, 0,
                                         0,       0, 0,        0, 0, 0},
                                        -6.10209e7)) != result.end());
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                        0)) != result.end());
  }

  SECTION("With squark vevs") {
    auto result = statconds.SolveForFields(
        testpars, {"vhur0", "vhdr0", "vulr3", "vurr3", "vdlr3", "vdrr3"});
    size_t nsol = result.size();

    INFO("This test is numerically unreliable, but I have never seen it find "
         "fewer than 11 solutions or miss the ew-like ones.")
    REQUIRE(nsol >= 11);
    REQUIRE(std::find(
                result.begin(), result.end(),
                Fieldspace::Point({245.94, 0, 11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                                   0,      0, 0,       0, 0, 0, 0, 0, 0, 0, 0},
                                  -6.10209e7)) != result.end());
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({-245.94, 0, -11.7114, 0, 0, 0, 0, 0,
                                         0,       0, 0,        0, 0, 0, 0, 0,
                                         0,       0, 0,        0, 0, 0},
                                        -6.10209e7)) != result.end());
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                        0)) != result.end());
  }
}

TEST_CASE("MSSM statconds generic scaling",
          "[unit][mssm][statconds][hom4ps2][gscaling]") {
  StationarityConditions<Models::MSSM, Scaling::Generic, Solver::Hom4ps2>
      statconds;
  std::vector<double> testpars{
      0.36151919899999996, // GAUGE_1
      0.63675145,          // GAUGE_2
      1.04060771,          // GAUGE_3
      -7155.0,             // HMIX_1
      246.21845810181637,  // HMIX_3
      863041.0,            // HMIX_4
      1.5232132235179132,  // HMIX_10
      0.8309101757104086,  // YU_3_3
      0.2319447148681335,  // YD_3_3
      0,                   // YE_3_3
      -50328892.06354008,  // MSOFT_21
      -51196116.93645992,  // MSOFT_22
      8743849.0,           // MSQ2_3_3
      8743849.0,           // MSU2_3_3
      8743849.0,           // MSD2_3_3
      8743849.0,           // MSL2_3_3
      8743849.0,           // MSE2_3_3
      3438.306307089671,   // TU_3_3
      1349.148564788819,   // TD_3_3
      0.,                  // TE_3_3
  };

  SECTION("Minimal field content") {
    auto result = statconds.SolveForFields(testpars, {"vhur0", "vhdr0"});
    std::sort(result.begin(), result.end());
    REQUIRE(3 == result.size());
    REQUIRE(result[0] ==
            Fieldspace::Point({245.94, 0, 11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                               0,      0, 0,       0, 0, 0, 0, 0, 0, 0, 0},
                              -6.10209e7));
    REQUIRE(result[1] ==
            Fieldspace::Point({-245.94, 0, -11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                               0,       0, 0,        0, 0, 0, 0, 0, 0, 0, 0},
                              -6.10209e7));
    REQUIRE(result[2] == Fieldspace::Point({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                           0));
  }

  SECTION("With squark vevs") {
    auto result = statconds.SolveForFields(
        testpars, {"vhur0", "vhdr0", "vulr3", "vurr3", "vdlr3", "vdrr3"});
    std::sort(result.begin(), result.end());
    REQUIRE(result.size() == 27);
    REQUIRE(result[16] ==
            Fieldspace::Point({245.94, 0, 11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                               0,      0, 0,       0, 0, 0, 0, 0, 0, 0, 0},
                              -6.10209e7));
    REQUIRE(result[17] ==
            Fieldspace::Point({-245.94, 0, -11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                               0,       0, 0,        0, 0, 0, 0, 0, 0, 0, 0},
                              -6.10209e7));
    REQUIRE(result[18] == Fieldspace::Point({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                            0));
    REQUIRE(result[0] ==
            Fieldspace::Point({36873.5, 0, 41169.1,  0, 0, 0, 0, 0, 0, 0, 0, 0,
                               25374.8, 0, -25962.2, 0, 0, 0, 0, 0, 0, 0},
                              -1.037934e+16));
    REQUIRE(result[1] ==
            Fieldspace::Point({36873.5,  0, 41169.1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                               -25374.8, 0, 25962.2, 0, 0, 0, 0, 0, 0, 0},
                              -1.037934e+16));
  }
}

TEST_CASE("MSSM with and without gscaling",
          "[functional][MSSM][gscaling][statconds][HOM4PS2]") {
  StationarityConditions<Models::MSSM, Scaling::Unscaled, Solver::Hom4ps2>
      noScale;
  StationarityConditions<Models::MSSM, Scaling::Generic, Solver::Hom4ps2>
      withScale;

  std::vector<double> testpars{
      0.36151919899999996, // GAUGE_1
      0.63675145,          // GAUGE_2
      1.04060771,          // GAUGE_3
      7155.0,              // HMIX_1
      246.21845810181637,  // HMIX_3
      863041.0,            // HMIX_4
      1.5232132235179132,  // HMIX_10
      0.8309101757104086,  // YU_3_3
      0.2319447148681335,  // YD_3_3
      0,                   // YE_3_3
      -50328892.06354008,  // MSOFT_21
      -51196116.93645992,  // MSOFT_22
      8743849.0,           // MSQ2_3_3
      8743849.0,           // MSU2_3_3
      8743849.0,           // MSD2_3_3
      8743849.0,           // MSL2_3_3
      8743849.0,           // MSE2_3_3
      3438.306307089671,   // TU_3_3
      1349.148564788819,   // TD_3_3
      0.,                  // TE_3_3
  };

  auto resScalingNone =
      noScale.SolveForFields(testpars, {"vhur0", "vhdr0", "vdlr3", "vdrr3"});
  auto resScalingGeneric =
      withScale.SolveForFields(testpars, {"vhur0", "vhdr0", "vdlr3", "vdrr3"});

  for (const auto &x : resScalingNone) {
    REQUIRE(std::find(resScalingGeneric.begin(), resScalingGeneric.end(), x) !=
            resScalingGeneric.end());
  }
}
