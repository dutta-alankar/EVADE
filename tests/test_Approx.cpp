#include "EVADE/Utilities/Approx.hpp"
#include "catch.hpp"

#include <limits>

using EVADE::Utilities::Approx;

TEST_CASE("Approx equality", "[unit][approx]") {
  SECTION("trivial inequalities") {
    REQUIRE_FALSE(Approx(1.) == 0.);
    REQUIRE_FALSE(Approx(1.) == -1.);
    REQUIRE_FALSE(Approx(1.) == 2.);
    REQUIRE(Approx(1.) != 0.);
    REQUIRE(Approx(1.) != -1.);
    REQUIRE(Approx(1.) != 2.);
  }

  SECTION("one and epsilon comparisons") {
    REQUIRE(Approx(1.) == 1.);
    REQUIRE(Approx(1.) == 1. + std::numeric_limits<double>::epsilon());
  }

  SECTION("zero comparisons") {
    REQUIRE(Approx(0.) == 0.);
    REQUIRE(Approx(0.) == -0.);
    REQUIRE_FALSE(Approx(0.) == std::numeric_limits<double>::denorm_min());
  }

  SECTION("max comparisons") {
    REQUIRE(Approx(std::numeric_limits<double>::max()) ==
            std::numeric_limits<double>::max());
    REQUIRE(Approx(std::numeric_limits<double>::min()) ==
            std::numeric_limits<double>::min());
  }

  SECTION("non-numbers") {
    REQUIRE(Approx(std::numeric_limits<double>::infinity()) ==
            std::numeric_limits<double>::infinity());
    REQUIRE_FALSE(Approx(std::numeric_limits<double>::infinity()) ==
                  -std::numeric_limits<double>::infinity());
    REQUIRE(Approx(std::numeric_limits<double>::infinity()) !=
            -std::numeric_limits<double>::infinity());
    REQUIRE_FALSE(Approx(std::numeric_limits<double>::quiet_NaN()) ==
                  std::numeric_limits<double>::quiet_NaN());
    REQUIRE(Approx(std::numeric_limits<double>::quiet_NaN()) !=
            std::numeric_limits<double>::quiet_NaN());
  }
}

TEST_CASE("Approx less", "[unit][approx]") {
  SECTION("trivial comparisons") {
    REQUIRE(Approx(0.) < 1.);
    REQUIRE(Approx(-1.) < 1.);
    REQUIRE(Approx(1.) < 2.);
    REQUIRE_FALSE(Approx(1.) < 0.);
    REQUIRE_FALSE(Approx(1.) < -1.);
    REQUIRE_FALSE(Approx(2.) < 1.);
  }

  SECTION("non-numbers") {
    REQUIRE(Approx(std::numeric_limits<double>::max()) <
            std::numeric_limits<double>::infinity());
    REQUIRE(Approx(-std::numeric_limits<double>::infinity()) <
            std::numeric_limits<double>::infinity());
  }
}

TEST_CASE("Approx tolerances", "[unit][approx]") {
  SECTION("Absolute Tolerance") {
    REQUIRE(Approx(0.).Abs(1.) == 1.);
    REQUIRE(Approx(0.).Abs(1.) < 1.1);
    REQUIRE(Approx(0.).Abs(-1.) == 1.);
    REQUIRE(Approx(1.).Abs(std::numeric_limits<double>::max()) ==
            std::numeric_limits<double>::max());
    REQUIRE(Approx(1.).Abs(std::numeric_limits<double>::max()) >
            std::numeric_limits<double>::lowest());
    SECTION("non-numbers") {
      REQUIRE(Approx(0.).Abs(std::numeric_limits<double>::max()) <
              std::numeric_limits<double>::infinity());
      REQUIRE(Approx(1.).Abs(std::numeric_limits<double>::infinity()) ==
              std::numeric_limits<double>::infinity());
      REQUIRE(Approx(1.).Abs(std::numeric_limits<double>::quiet_NaN()) < 1.1);
    }
  }
  SECTION("Relative Tolerance") {
    REQUIRE(Approx(1.).Rel(0.1) == 1.1);
    REQUIRE(Approx(-1.).Rel(0.1) == -1.1);
    REQUIRE(Approx(1.).Rel(0.1) < 1.2);
    REQUIRE(Approx(0.).Rel(1.) == 1.);
    REQUIRE(Approx(0.).Rel(1.) == std::numeric_limits<double>::max());
    REQUIRE_FALSE(Approx(std::numeric_limits<double>::lowest()).Rel(1.) ==
                  std::numeric_limits<double>::max());
    REQUIRE(Approx(std::numeric_limits<double>::lowest()).Rel(2.) ==
            std::numeric_limits<double>::max());

    SECTION("non-numbers") {
      REQUIRE(Approx(std::numeric_limits<double>::max()).Rel(2.) <
              std::numeric_limits<double>::infinity());
      REQUIRE(Approx(std::numeric_limits<double>::lowest()).Rel(2.) >
              -std::numeric_limits<double>::infinity());
      REQUIRE(Approx(1.).Rel(std::numeric_limits<double>::quiet_NaN()) < 1.1);
      REQUIRE(Approx(1.).Rel(std::numeric_limits<double>::infinity()) ==
              std::numeric_limits<double>::max());
    }
  }
}
