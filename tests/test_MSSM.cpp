#include "EVADE/BouncePolicies/Quartic.hpp"
#include "EVADE/Models/MSSM.hpp"
#include "EVADE/ScalingPolicies/Generic.hpp"
#include "EVADE/SolverPolicies/Hom4ps2.hpp"
#include "EVADE/StationarityConditions.hpp"
#include "EVADE/TunnellingCalculator.hpp"
#include "EVADE/config.h"
#include "ParameterReader.hpp"
#include "StabilityOutput.hpp"
#include "catch.hpp"

#include <algorithm>
#include <cmath>

using namespace EVADE;

TEST_CASE("MSSM full calculation", "[functional][mssm][hom4ps2][gscaling]") {
  StationarityConditions<Models::MSSM, Scaling::Generic, Solver::Hom4ps2>
      statconds;
  std::vector<double> testpars{
      0.36151919899999996, // GAUGE_1
      0.63675145,          // GAUGE_2
      1.04060771,          // GAUGE_3
      -7155.0,             // HMIX_1
      246.21845810181637,  // HMIX_3
      863041.0,            // HMIX_4
      1.5232132235179132,  // HMIX_10
      0.8309101757104086,  // YU_3_3
      0.2319447148681335,  // YD_3_3
      0,                   // YE_3_3
      -50328892.06354008,  // MSOFT_21
      -51196116.93645992,  // MSOFT_22
      8743849.0,           // MSQ2_3_3
      8743849.0,           // MSU2_3_3
      8743849.0,           // MSD2_3_3
      8743849.0,           // MSL2_3_3
      8743849.0,           // MSE2_3_3
      3438.306307089671,   // TU_3_3
      1349.148564788819,   // TD_3_3
      0.,                  // TE_3_3
  };

  auto statpoints = statconds.SolveForFields(
      testpars, {"vhur0", "vhdr0", "vulr3", "vurr3", "vdlr3", "vdrr3"});

  TunnellingCalculator<Models::MSSM, Bounce::Quartic> test(testpars);

  std::vector<Fieldspace::TunnellingDir> result;
  for (auto &x : statpoints) {
    REQUIRE_NOTHROW(result.emplace_back(test.CalculateBounce(x)));
  }
  result.erase(std::remove_if(result.begin(), result.end(),
                              [](const Fieldspace::TunnellingDir &x) -> bool {
                                return x.B() < 0;
                              }),
               result.end());
  std::sort(result.begin(), result.end());
  REQUIRE(result.size() == 16);

  auto initfields = Models::MSSM::InitialVacuum(testpars);
  Fieldspace::Point init{initfields, Models::MSSM::V(testpars, initfields)};
  Fieldspace::Point target1{{5028.61, 0, 19818.1,  0, 0, 0, 0, 0,
                             12806.8, 0, -13592.6, 0, 0, 0, 0, 0,
                             0,       0, 0,        0, 0, 0},
                            -3.31247e+15};
  Fieldspace::Point target2{{36873.5, 0, 41169.1,  0, 0, 0, 0, 0, 0, 0, 0, 0,
                             25374.8, 0, -25962.2, 0, 0, 0, 0, 0, 0, 0},
                            -1.037934e+16};
  for (const auto &x : result) {
    WARN(x.Target());
  }

  Fieldspace::Direction d1(init, std::move(target1));
  Fieldspace::Direction d2(init, std::move(target2));
  REQUIRE(result[0] == Fieldspace::TunnellingDir(249.155, d1));
  REQUIRE(result[12] == Fieldspace::TunnellingDir(3291.94, d2));
}

TEST_CASE("MSSM full testpoints",
          "[functional][mssm][hom4ps2][gscaling][reader][writer]") {
  ParameterReader reader(TESTDIR + std::string("/testdata_names.in"),
                         Models::MSSM::ParameterNames());

  StationarityConditions<Models::MSSM, Scaling::Generic, Solver::Hom4ps2>
      statconds;

  StabilityOutput writer("test.tsv", OutputType::stability,
                         Models::MSSM::FieldNames());

  std::vector<double> testpars;
  std::string pointID;

  while (reader.GetPoint(pointID, testpars)) {
    auto statpoints = statconds.SolveForFields(
        testpars, {"vhur0", "vhdr0", "vulr3", "vurr3", "vdlr3", "vdrr3"});
    {
      INFO("EW vacuum + trivial vacuum have to exist");
      REQUIRE(statpoints.size() >= 2);
    }
    TunnellingCalculator<Models::MSSM, Bounce::Quartic> test(testpars);
    std::vector<Fieldspace::TunnellingDir> result;

    {
      INFO("calculating bounce and writing result");
      for (auto &x : statpoints) {
        REQUIRE_NOTHROW(result.emplace_back(test.CalculateBounce(x)));
      }
      REQUIRE_NOTHROW(writer.Write(pointID, result));
    }
    WARN(pointID);
    result.erase(std::remove_if(result.begin(), result.end(),
                                [](const Fieldspace::TunnellingDir &x) -> bool {
                                  return x.B() < 0;
                                }),
                 result.end());
    std::sort(result.begin(), result.end());

    {
      INFO("checking validity of result");
      REQUIRE(result.size() > 0);
    }
  }
}
