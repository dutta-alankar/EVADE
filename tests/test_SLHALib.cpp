#include "EVADE/Models/MSSM.hpp"
#include "EVADE/config.h"
#include "catch.hpp"
#include "CSLHA.h"

TEST_CASE("read and use test slha", "[unit][slhalib]") {
  std::complex<double> slhadata[nslhadata];
  std::string file(TESTDIR);
  file += "/testslha.slha";
  int error = 0;
  SLHARead(&error, slhadata, file.c_str(), 0);
  REQUIRE(error==0);
  auto pars = EVADE::Models::MSSM::ParametersFromSLHA(slhadata);
  REQUIRE(pars.size()==EVADE::Models::MSSM::ParameterNames().size());
  REQUIRE(pars[0]==Gauge_g1);
  REQUIRE(pars[0]==CatchApprox(0.3615192));
  REQUIRE(pars[1]==Gauge_g2);
  REQUIRE(pars[2]==Gauge_g3);
  REQUIRE(pars[3]==HMix_MUE);
  REQUIRE(pars[4]==HMix_VEV);
  REQUIRE(pars[5]==HMix_MA02);
  REQUIRE(std::tan(pars[6])==CatchApprox(MinPar_TB.real()));
  REQUIRE(pars[7]==Yu_Yt);
  REQUIRE(pars[8]==Yd_Yb);
  REQUIRE(pars[9]==Ye_Ytau);
  REQUIRE(pars[10]==MSoft_MHd2);
  REQUIRE(pars[10]==CatchApprox(-3242272.49931));
  REQUIRE(pars[11]==MSoft_MHu2);
  REQUIRE(pars[12]==MSQ2_MSQ2(3,3));
  REQUIRE(pars[13]==MSU2_MSU2(3,3));
  REQUIRE(pars[14]==MSD2_MSD2(3,3));
  REQUIRE(pars[15]==MSL2_MSL2(3,3));
  REQUIRE(pars[16]==MSE2_MSE2(3,3));
  REQUIRE(pars[17]==Tu_Tf(3,3));
  REQUIRE(pars[17]==CatchApprox(-1500));
  REQUIRE(pars[18]==Td_Tf(3,3));
  REQUIRE(pars[19]==Te_Tf(3,3));
}
