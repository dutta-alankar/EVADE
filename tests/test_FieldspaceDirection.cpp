#include "EVADE/Fieldspace/BfBDir.hpp"
#include "EVADE/Fieldspace/Direction.hpp"
#include "EVADE/Fieldspace/TunnellingDir.hpp"
#include "EVADE/Utilities/Utilities.hpp"
#include "catch.hpp"
#include <algorithm>

TEST_CASE("Direction constructor", "[unit][fieldspace][direction]") {
  using Catch::Detail::Approx;
  using EVADE::Fieldspace::Direction;
  using EVADE::Fieldspace::Point;
  using EVADE::Utilities::Norm;
  using EVADE::Utilities::NormalizedDirection;

  std::vector<double> testfields{10., 20., 30., 40., 50.};
  std::vector<double> origin(testfields.size(), 0.);

  Point p(testfields, -10);
  Point o(origin, 0);

  Direction d(o, p);

  REQUIRE(d.Distance() == Approx(Norm(testfields)));
  auto normd = NormalizedDirection(o.Fields(), p.Fields());
  for (size_t i = 0; i != o.Fields().size(); ++i) {
    REQUIRE(d.Dir()[i] == Approx(normd[i]));
  }

  REQUIRE_NOTHROW(d.ToString());
}

TEST_CASE("Direction comparison", "[unit][fieldspace][direction]") {
  using EVADE::Fieldspace::Direction;
  using EVADE::Fieldspace::Point;

  Point p0({0, 0, 0, 0, 0}, 0);
  Point p1({1, 2, 3, 4, 5}, 660);
  Point p2({5, -1, 4, -2, 3}, -1000);
  Point p3({50, -1, 4, -2, 3}, -1000);

  Direction d01(p0, p1);
  Direction d10(p1, p0);
  Direction d02(p0, p2);
  Direction d20(p2, p0);
  Direction d12(p1, p2);
  Direction d03(p0, p3);

  SECTION("Equal") {
    REQUIRE(d01 == d01);
    REQUIRE(d12 == d12);
    REQUIRE(d01 != d02);
    REQUIRE(d12 != d02);
    REQUIRE(d01 != d10);
  }

  SECTION("Comparison") {
    REQUIRE(d02 < d01);
    REQUIRE(d02 < d12);
    REQUIRE(d20 < d02);
    REQUIRE(d02 < d03);
  }
}

TEST_CASE("Tunnelling Direction", "[unit][fieldspace][direction]") {
  using EVADE::Fieldspace::Direction;
  using EVADE::Fieldspace::Point;
  using EVADE::Fieldspace::TunnellingDir;

  Point p0({0, 0, 0, 0, 0}, 0);
  Point p1({1, 2, 3, 4, 5}, 660);
  Point p2({5, -1, 4, -2, 3}, -1000);
  Point p3({5, -1, 4, -2, 3}, 1000);

  Direction d01(p0, p1);
  Direction d10(p1, p0);
  Direction d02(p0, p2);
  Direction d03(p0, p3);

  TunnellingDir td1(100, d01);
  TunnellingDir td2(10, d01);
  TunnellingDir td3(10, d02);
  TunnellingDir td4(10.00000001, d02);
  TunnellingDir tdi1(-1, d01);
  TunnellingDir tdi2(-2, d01);
  TunnellingDir tdi3(-2, d02);
  TunnellingDir td5(8, d03);

  SECTION("Equal") {
    REQUIRE(td1 == td1);
    REQUIRE(td2 == td2);
    REQUIRE(td3 == td3);
    REQUIRE(td3 == td4);
    REQUIRE(tdi1 == tdi1);
    REQUIRE_FALSE(td1 == td2);
    REQUIRE(tdi1 == tdi2);
  }

  SECTION("Not Equal") {
    REQUIRE(td1 != td2);
    REQUIRE(td1 != td3);
    REQUIRE(td3 != td2);
    REQUIRE_FALSE(td1 != td1);
    REQUIRE(tdi1 != tdi3);
    REQUIRE(tdi2 != tdi3);
    REQUIRE(td5 != td3);
  }

  SECTION("Comparison") {
    REQUIRE(td1 > td2);
    REQUIRE(td2 < td1);
    REQUIRE(td3 < td1);

    REQUIRE(d01 > d02);
    REQUIRE(td2 > td3);
    REQUIRE(tdi1 > tdi3);
    REQUIRE(tdi2 > tdi3);
    REQUIRE(td1 < tdi1);
    REQUIRE(td1 < tdi2);
    REQUIRE(td1 < tdi3);
    REQUIRE(d02 < d03);
    REQUIRE(td5 < td2);
  }

  SECTION("Algorithms") {
    std::vector<TunnellingDir> test{td1, td2, td3, td4, tdi1, tdi2, tdi3, td5};
    REQUIRE(*std::min_element(test.begin(), test.end()) == td5);
    std::sort(test.begin(), test.end());
    REQUIRE(test[0] == td5);
    REQUIRE(test[1] == td3);
  }
}

TEST_CASE("BfB Directions", "[unit][fieldspace][direction]") {
  using EVADE::Fieldspace::BfBDir;
  using EVADE::Fieldspace::Direction;
  using EVADE::Fieldspace::Point;

  Point p0({0, 0, 0, 0, 0}, 0);
  Point p1({1, 2, 3, 4, 5}, 660);
  Point p2({5, -1, 4, -2, 3}, -1000);

  Direction d01(p0, p1);
  Direction d10(p1, p0);
  Direction d02(p0, p2);

  BfBDir td1(10, d01);
  BfBDir td2(1, d01);
  BfBDir td3(1, d02);
  BfBDir td4(1.00000001, d02);

  SECTION("Equal") {
    REQUIRE(td1 == td1);
    REQUIRE(td2 == td2);
    REQUIRE(td3 == td3);
    REQUIRE(td3 == td4);
    REQUIRE_FALSE(td1 == td2);
  }

  SECTION("Not Equal") {
    REQUIRE(td1 != td2);
    REQUIRE(td1 != td3);
    REQUIRE(td3 != td2);
    REQUIRE_FALSE(td1 != td1);
  }

  SECTION("Comparison") {
    REQUIRE(td1 > td2);
    REQUIRE(td2 < td1);
    REQUIRE(td3 < td1);

    REQUIRE(d01 > d02);
    REQUIRE(td2 > td3);
  }
}
