#ifndef EVADE_CONFIGREADER_H
#define EVADE_CONFIGREADER_H

#include "libconfig.h++"

#include <string>
#include <vector>

enum class OutputType;

/**
 * This class taked care of reading the requires entries fron the  configuration
 * file. This class uses [libconfig](https://hyperrealm.github.io/libconfig/)
 * to do all the work. The syntax for the configuration file is explained
 * [here](https://hyperrealm.github.io/libconfig/libconfig_manual.html#Configuration-Files).
 */
class ConfigReader {
  libconfig::Config cfg;

public:
  /**
   * Construct an object reading fron the given configuration file.
   * @param file the configuration file
   */
  ConfigReader(const std::string &file);

  /**
   * Gets the path to the input file from the configuration and saves it into
   * the given string. The setting is required to be at:
   * ```
   * EVADE {
   *   input = "..."
   * }
   * ```
   * @param  input path to input file will be put in here
   * @return       whether the setting was succesfully found
   */
  bool GetInputFile(std::string &input) const;

  /**
   * Gets the path to the output file from the configuration and saves it into
   * the given string. The setting is required to be at:
   * ```
   * EVADE {
   *   output = "..."
   * }
   * ```
   * @param  output path to output file will be put in here
   * @return        whether the setting was succesfully found
   */
  bool GetOutputFile(std::string &output) const;

  /**
   * Gets the sets of non-zero fields to use when calcualting the stationary
   * points from the configuration and saves them into the given vector. The
   * setting is required to be at:
   * ```
   * EVADE {
   *    StationarityConditions {
   *      fielsSets = ( ["x1","x2",...], [...], ... )
   *    }
   * }
   * ```
   * @param  fieldSets the obtained fieldSets
   * @return           whether the setting was succesfully found
   */
  bool GetFieldSets(std::vector<std::vector<std::string>> &fieldSets) const;

  /**
   * Gets the path to the HOM4PS2 directory from the configuration and saves it
   * into the given string. The setting is required to be at:
   * ```
   * EVADE {
   *   SolverPolicies {
   *      Hom4ps2 {
   *          path = "..."
   *      }
   *   }
   * }
   * ```
   * @param  path path to the HOM4PS2 directory will be put in here
   * @return      whether the setting was succesfully found
   */
  bool GetHom4ps2Path(std::string &path) const;

  /**
   * Gets the number of Bertini MPI threads from the configuration and saves it
   * into the given int. A value <=1 uses serial processing.
   * The setting is required to be at:
   * ```
   * EVADE {
   *   SolverPolicies {
   *      Bertini {
   *          nThreads = x
   *      }
   *   }
   * }
   * ```
   * @param  nThreads number of threads
   * @return          whether the setting was succesfully found
   */
  bool GetBertiniThreads(int &nThreads) const;

  /**
   * Gets the path to the Bertini working directory from the configuration and
   * saves it into the given string. This is **not** the path to the executable,
   * but the path to the directory where you want the Bertini output files to
   * end up. The setting is required to be at:
   * ```
   * EVADE {
   *   SolverPolicies {
   *     Bertini {
   *       path = "..."
   *     }
   *   }
   * }
   * ```
   * @param  path path to the HOM4PS2 directory will be put in here
   * @return      whether the setting was succesfully found
   */
  bool GetBertiniPath(std::string &path) const;

  /**
  * Get the requested output type.
  * The output type is a string and can be:
  *  - stability: Uses StabilityOutput, for parameter scans
  *  - vevs: Uses VevOutput, for detailes studies of vacuum behaviour
  * The setting is required to be at:
  * ```
  * EVADE {
  *   outputType = "..."
  * }
  * ```
  * @param  type enum specifying the selected type
  * @return      whether the setting was succesfully found
  */
  bool GetOutputType(OutputType &type) const;
};

#endif
