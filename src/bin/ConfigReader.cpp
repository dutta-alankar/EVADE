#include "ConfigReader.hpp"

#include "StabilityOutput.hpp"

#include <algorithm>
#include <iostream>
#include <stdexcept>

ConfigReader::ConfigReader(const std::string &file) {
  try {
    cfg.readFile(file.c_str());
  } catch (const libconfig::FileIOException &fioex) {
    std::cerr << "I/O error while reading configuration file." << std::endl;
    throw std::runtime_error("Invalid configuration file.");
  } catch (const libconfig::ParseException &pex) {
    std::cerr << "Configuration parse error at " << pex.getFile() << ":"
              << pex.getLine() << " - " << pex.getError() << std::endl;
    throw std::runtime_error("Invalid configuration file.");
  }
}

bool ConfigReader::GetInputFile(std::string &input) const {
  try {
    input = cfg.getRoot()["EVADE"]["input"].c_str();
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "Input file name not found at " << nfex.getPath() << "."
              << std::endl;
    return false;
  }
  return true;
}

bool ConfigReader::GetOutputFile(std::string &output) const {
  try {
    output = cfg.getRoot()["EVADE"]["output"].c_str();
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "Output file name not found at " << nfex.getPath() << "."
              << std::endl;
    return false;
  }
  return true;
}

bool ConfigReader::GetFieldSets(
    std::vector<std::vector<std::string>> &fieldSets) const {
  try {
    for (const auto &set :
         cfg.getRoot()["EVADE"]["StationarityConditions"]["fieldSets"]) {
      fieldSets.emplace_back(set.getLength());
      std::transform(set.begin(), set.end(), fieldSets.back().begin(),
                     [](const libconfig::Setting &str) { return str.c_str(); });
    }
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "FieldSets not found at " << nfex.getPath() << "."
              << std::endl;
    return false;
  }
  return true;
}

bool ConfigReader::GetHom4ps2Path(std::string &path) const {
  try {
    path = cfg.getRoot()["EVADE"]["SolverPolicies"]["Hom4ps2"]["path"].c_str();
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "HOM4PS2 path not found at " << nfex.getPath() << "."
              << std::endl;
    return false;
  }
  return true;
}

bool ConfigReader::GetBertiniThreads(int &nThreads) const {
  try {
    nThreads = cfg.getRoot()["EVADE"]["SolverPolicies"]["Bertini"]["nThreads"];
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "Bertini thread count not found at " << nfex.getPath() << "."
              << std::endl;
    return false;
  }
  return true;
}

bool ConfigReader::GetBertiniPath(std::string &path) const {
  try {
    path = cfg.getRoot()["EVADE"]["SolverPolicies"]["Bertini"]["path"].c_str();
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "Bertini working directory path not found at "
              << nfex.getPath() << "." << std::endl;
    return false;
  }
  return true;
}

bool ConfigReader::GetOutputType(OutputType &type) const {
  std::string buffer;
  try {
    buffer = cfg.getRoot()["EVADE"]["outputType"].c_str();
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "OutputType not found at " << nfex.getPath() << "."
              << std::endl;
    return false;
  }
  if (buffer == "stability") {
    type = OutputType::stability;
  } else if (buffer == "vevs") {
    type = OutputType::vevs;
  } else {
    std::cerr << "Invalid output type " << buffer << " requested." << std::endl;
    return false;
  }

  return true;
}
