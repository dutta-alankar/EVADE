#include "StabilityOutput.hpp"

#include "EVADE/Fieldspace/TunnellingDir.hpp"

#include <algorithm>
#include <stdexcept>

StabilityOutput::StabilityOutput(const std::string &filepath, OutputType type,
                                 const std::vector<std::string> &fieldnames)
    : file_{filepath}, type_{type} {
  if (file_.fail()) {
    throw std::runtime_error("StabilityOutput cannot open the output file " +
                             filepath);
  }
  switch (type_) {
  case OutputType::stability:
    HeaderStability(fieldnames);
    break;
  case OutputType::vevs:
    HeaderVevs(fieldnames);
    break;
  default:
    throw std::runtime_error("Invalid output type requested.");
  }
}

void StabilityOutput::Write(
    const std::string &pointID,
    const std::vector<EVADE::Fieldspace::TunnellingDir> &results) {
  switch (type_) {
  case OutputType::stability:
    WriteStability(pointID, results);
    break;
  case OutputType::vevs:
    WriteVevs(pointID, results);
    break;
  default:
    throw std::runtime_error("Invalid output type requested.");
  }
}

void StabilityOutput::HeaderStability(
    const std::vector<std::string> &fieldnames) {
  file_ << "index\tstat_nTot\tstat_nDeep\tstat_nDeg\tstat_nUnbounded";
  file_ << "\tfast_B";
  file_ << "\tinit_V";
  for (const auto &x : fieldnames) {
    file_ << "\tinit_" << x;
  }

  file_ << "\tfast_V";
  for (const auto &x : fieldnames) {
    file_ << "\tfast_" << x;
  }

  file_ << "\tdeep_B\tdeep_V";
  for (const auto &x : fieldnames) {
    file_ << "\tdeep_" << x;
  }
  file_ << std::endl;
}

#include <iostream>
void StabilityOutput::WriteStability(
    const std::string &pointID,
    const std::vector<EVADE::Fieldspace::TunnellingDir> &results) {
  using EVADE::Fieldspace::IO::PrintInvalidBounces;
  using EVADE::Fieldspace::IO::PrintLabels;
  using EVADE::Fieldspace::IO::PrintOrigin;

  file_ << pointID << "\t";
  for (size_t x : CountStatistics(results)) {
    file_ << x << "\t";
  }

  file_ << GetFastest(results).ToString(
      PrintInvalidBounces::yes, PrintOrigin::yes, PrintLabels::no, 7, "\t")
        << "\t"
        << GetDeepest(results).ToString(PrintInvalidBounces::yes,
                                        PrintOrigin::no, PrintLabels::no, 7,
                                        "\t")
        << std::endl;
}

void StabilityOutput::HeaderVevs(const std::vector<std::string> &fieldnames) {
  file_ << "index\tB\tV";
  for (const auto &x : fieldnames) {
    file_ << "\t" << x;
  }
  file_ << std::endl;
}

void StabilityOutput::WriteVevs(
    const std::string &pointID,
    const std::vector<EVADE::Fieldspace::TunnellingDir> &results) {
  using EVADE::Fieldspace::IO::PrintInvalidBounces;
  using EVADE::Fieldspace::IO::PrintLabels;
  using EVADE::Fieldspace::IO::PrintOrigin;
  for (const auto &v : results) {
    file_ << pointID << "\t"
          << v.ToString(PrintInvalidBounces::yes, PrintOrigin::no,
                        PrintLabels::no, 7, "\t")
          << "\n";
  }
  std::flush(file_);
}

const EVADE::Fieldspace::TunnellingDir &
GetFastest(const std::vector<EVADE::Fieldspace::TunnellingDir> &points) {
  return *std::min_element(points.begin(), points.end());
}

const EVADE::Fieldspace::TunnellingDir &
GetDeepest(const std::vector<EVADE::Fieldspace::TunnellingDir> &points) {
  using EVADE::Fieldspace::Direction;
  using EVADE::Fieldspace::TunnellingDir;

  auto lessNoBounce = [](const TunnellingDir &a, const TunnellingDir &b) {
    return static_cast<Direction>(a) < static_cast<Direction>(b);
  };

  return *std::min_element(points.begin(), points.end(), lessNoBounce);
}

std::vector<size_t>
CountStatistics(const std::vector<EVADE::Fieldspace::TunnellingDir> &points) {
  using EVADE::BounceCode;
  using EVADE::Fieldspace::TunnellingDir;

  auto isInitialOrDegenerate = [](const TunnellingDir &x) {
    double b = x.B();
    if (b > 0)
      return false;
    return (static_cast<BounceCode>(b) == BounceCode::initial) ||
           (static_cast<BounceCode>(b) == BounceCode::initDegenerate);
  };
  size_t nDegenerate =
      std::count_if(points.begin(), points.end(), isInitialOrDegenerate);

  auto tunnellingPossible = [](const TunnellingDir &x) { return x.B() >= 0.; };
  size_t nDeep =
      std::count_if(points.begin(), points.end(), tunnellingPossible);

  size_t nTot = points.size();
  auto isUnbounded = [](const TunnellingDir &x) {
    return static_cast<BounceCode>(x.B()) == BounceCode::unbounded;
  };

  size_t nUnbounded = std::count_if(points.begin(), points.end(), isUnbounded);

  return {nTot, nDeep, nDegenerate, nUnbounded};
}
