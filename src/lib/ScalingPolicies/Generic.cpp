#include "EVADE/ScalingPolicies/Generic.hpp"

#include "EVADE/Utilities/Polynomial.hpp"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <Eigen/Core>
#include <Eigen/QR>
#include <stdexcept>
#include <string>
#include <utility>

void EVADE::Scaling::Generic::ScaleSystem(
    std::vector<Utilities::Polynomial> &equations) {
  std::vector<Utilities::Polynomial> tmpEq;
  // drop terms with zero coefficients and logtransform the remaining
  // coefficients
  for (const auto &poly : equations) {
    if (equations.size() != poly.begin()->first.size()) {
      throw std::out_of_range(
          "Can only scale system with as many equations as variabels (" +
          std::to_string(equations.size()) + " vs " +
          std::to_string(poly.begin()->first.size()) + ").");
    }
    Utilities::Polynomial tmpPoly;
    for (const auto &term : poly) {
      if (std::abs(term.second) > 0) {
        tmpPoly[term.first] = std::log10(std::abs(term.second));
      }
    }
    tmpEq.emplace_back(std::move(tmpPoly));
  }

  // Eigen uses signed indices, we use casts to avoid sign-conversion warnings
  int N = static_cast<int>(tmpEq.size());

  // generate the matrix alpha
  Eigen::MatrixXd alpha = Eigen::MatrixXd::Zero(2 * N, 2 * N);
  for (int i = 0; i != N; i++) {
    alpha(i, i) = static_cast<double>(tmpEq[static_cast<size_t>(i)].size());
  }
  for (int s = 0; s != N; s++) {
    for (int i = 0; i != N; i++) {
      double sum = 0;
      for (const auto &term : tmpEq[static_cast<size_t>(i)]) {
        sum += term.first[static_cast<size_t>(s)];
      }
      alpha(N + s, i) = sum;
    }
  }
  for (int s = 0; s != N; s++) {
    for (int k = 0; k != N; k++) {
      double sum = 0;
      for (const auto &poly : tmpEq) {
        for (const auto &term : poly) {
          sum += term.first[static_cast<size_t>(s)] *
                 term.first[static_cast<size_t>(k)];
        }
      }
      alpha(N + s, N + k) = sum;
    }
  }
  for (int s = 0; s != N; s++) {
    for (int k = 0; k != N; k++) {
      double sum = 0;
      for (const auto &term : tmpEq[static_cast<size_t>(s)]) {
        sum += term.first[static_cast<size_t>(k)];
      }
      alpha(s, N + k) = sum;
    }
  }

  // generate the vector beta
  Eigen::VectorXd beta(2 * N);
  for (int s = 0; s != N; s++) {
    double sum = 0;
    for (const auto &term : tmpEq[static_cast<size_t>(s)]) {
      sum += term.second;
    }
    beta(s) = -sum;
  }
  for (int s = 0; s != N; s++) {
    double sum = 0;
    for (const auto &poly : tmpEq) {
      for (const auto &term : poly) {
        sum += term.second * term.first[static_cast<size_t>(s)];
      }
    }
    beta(N + s) = -sum;
  }

  // solve the linear system
  Eigen::VectorXd x = alpha.colPivHouseholderQr().solve(beta);

  // extract equation and variable scaling factors
  varscale_ = std::vector<double>(x.data() + N, x.data() + 2 * N);

  // calculate the scaled coefficients
  for (size_t i = 0; i != equations.size(); i++) {
    for (auto &term : equations[i]) {
      double DUM = std::abs(term.second);
      if (DUM == 0.0) {
        term.second = 0.0;
      } else {
        double sum = x[static_cast<int>(i)] + std::log10(DUM);
        for (size_t k = 0; k != equations.size(); k++) {
          sum += varscale_[k] * term.first[k];
        }
        term.second = std::copysign(std::pow(10, sum), term.second);
      }
    }
  }
}

void EVADE::Scaling::Generic::UnscaleSolutions(
    std::vector<std::vector<double>> &solutions) {
  for (auto &fields : solutions) {
    assert(varscale_.size() == fields.size());
    std::transform(fields.begin(), fields.end(), varscale_.begin(),
                   fields.begin(), [](double field, double scale) -> double {
                     return field * std::pow(10, scale);
                   });
  }
}
