#include "EVADE/SolverPolicies/Bertini.hpp"

#include "EVADE/Utilities/Polynomial.hpp"
#include "EVADE/config.h"
#include <algorithm>
#include <complex>
#include <fstream>
#include <iostream>
#include <limits>
#include <stdexcept>

EVADE::Solver::Bertini::Bertini(std::string path, int nThreads)
    : folder_{path + "/Bertini"}, nThreads_{nThreads} {
  folder_.SetCleanup({"failed_paths", "finite_solutions", "input", "main_data",
                      "midpath_data", "nonsingular_solutions", "output",
                      "raw_data", "raw_solutions", "real_finite_solutions",
                      "singular_solutions", "start"});
}

std::vector<std::vector<double>> EVADE::Solver::Bertini::Call(
    const std::vector<Utilities::Polynomial> &equations,
    const std::vector<std::string> &varnames) {
  WriteInput(equations, varnames);
  int status = Run();
  if (status != 0)
    status = Run(); // retry once
  if (status != 0)
    throw(std::runtime_error("Bertini returned non-zero exit status: " +
                             std::to_string(status)));
  return ReadResult(varnames);
}

std::vector<std::vector<double>>
EVADE::Solver::Bertini::ReadResult(const std::vector<std::string> &varnames) {
  std::ifstream ifile(folder_.Path() + "real_finite_solutions");
  if (ifile) {
    int nsol;
    ifile >> nsol;
    std::vector<std::vector<double>> result(
        nsol, std::vector<double>(varnames.size()));
    double dump;
    for (auto &solution : result) {
      for (auto &var : solution) {
        if (!ifile.good()) {
          throw std::runtime_error("Parse error in " + folder_.Path() +
                                   "real_finite_solutions");
        }
        ifile >> var >> dump;
      }
    }
    return result;
  } else {
    throw std::runtime_error("Bertini output file " + folder_.Path() +
                             "real_finite_solutions could not be opened.");
  }
}

int EVADE::Solver::Bertini::Run() {
  std::string command;
  command += "cd " + folder_.Path() + "; ";
  if (nThreads_ <= 1) {
    command += std::string(BERTINI_EXECUTABLE) + " >/dev/null";
  } else {
    command += "mpirun -np " + std::to_string(nThreads_) + " " +
               std::string(BERTINI_EXECUTABLE) + " >/dev/null";
  }
  return std::system(command.c_str());
}

void EVADE::Solver::Bertini::WriteInput(
    const std::vector<Utilities::Polynomial> &equations,
    const std::vector<std::string> &varnames) {
  std::ofstream ofile;
  ofile.open(folder_.Path() + std::string("input"));
  ofile << "CONFIG" << std::endl;
  ofile << "MPTYPE: 0;" << std::endl;
  ofile << "END;" << std::endl;
  ofile << "INPUT" << std::endl;
  // write variables
  ofile << "variable_group ";
  for (size_t i = 0; i != varnames.size(); ++i) {
    if (i != 0) {
      ofile << ", ";
    }
    ofile << varnames[i];
  }
  ofile << ";" << std::endl;
  // write function names
  ofile << "function ";
  std::vector<std::string> pnames(equations.size(), "f");
  for (size_t i = 0; i != pnames.size(); ++i) {
    pnames[i] += std::to_string(i);
    if (i != 0) {
      ofile << ", ";
    }
    ofile << pnames[i];
  }
  ofile << ";" << std::endl;
  // write functions
  for (size_t i = 0; i != pnames.size(); ++i) {
    ofile << pnames[i] << " = "
          << equations[i].ToString(varnames,
                                   std::numeric_limits<double>::max_digits10)
          << ";" << std::endl;
  }
  ofile << "END;" << std::endl;
}
