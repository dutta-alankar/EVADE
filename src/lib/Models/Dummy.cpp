#include "EVADE/Models/Dummy.hpp"

#include <cmath>

const std::vector<std::string> &EVADE::Models::Dummy::FieldNames() {
  static const std::vector<std::string> fieldNames{"x0", "x1"};
  return fieldNames;
}

EVADE::Utilities::Polynomial
EVADE::Models::Dummy::VPoly(const std::vector<double> &) {
  Utilities::Polynomial pot;
  pot[{2, 0}] = 2;
  pot[{1, 1}] = -4;
  pot[{0, 3}] = -2;
  return pot;
}

EVADE::Utilities::Polynomial
EVADE::Models::Dummy::VPoly4(const std::vector<double> &) {
  Utilities::Polynomial pot;
  pot[{2, 2}] = 2;
  pot[{1, 3}] = 1;
  pot[{3, 1}] = -1;
  pot[{0, 4}] = -3;
  return pot;
}

double EVADE::Models::Dummy::V(const std::vector<double> &,
                               const std::vector<double> &) {
  return -1000.;
}

std::vector<double>
EVADE::Models::Dummy::InitialVacuum(const std::vector<double> &) {
  return {1, 1};
}

EVADE::Models::Dummy::Dummy(const std::vector<double> &) {}

double EVADE::Models::Dummy::Msq(const std::vector<double> &) const {
  return 10;
}

double EVADE::Models::Dummy::A(const std::vector<double> &) const { return 1; }

double EVADE::Models::Dummy::Lam(const std::vector<double> &) const {
  return 0.01;
}
