#include "EVADE/BouncePolicies/Quartic.hpp"

#include "EVADE/Fieldspace/TunnellingDir.hpp"

static constexpr double pi = 3.141592653589793238462643383279502884197169399375;

#include <cmath>

double EVADE::Bounce::QuarticBounce(double msq, double A, double lam) {
  if (A < 0) {
    A = std::abs(A);
  }
  if (msq < 0) {
    return 0.; // no minimum at initial point
  }
  if (lam < 0) {
    return static_cast<double>(EVADE::BounceCode::unbounded);
  }
  if (msq > A * A / 4. / lam) {
    return static_cast<double>(EVADE::BounceCode::notDeeper);
  }

  using std::pow;
  static constexpr double al_1 = 13.832;
  static constexpr double al_2 = -10.819;
  static constexpr double al_3 = 2.0765;

  double delta = 8. * lam * msq / A / A;
  return pi * pi / 3. / lam * pow(2 - delta, -3) *
         (al_1 * delta + al_2 * pow(delta, 2) + al_3 * pow(delta, 3));
}
