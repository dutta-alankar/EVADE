#include "EVADE/Fieldspace/TunnellingDir.hpp"

#include "EVADE/Utilities/Approx.hpp"
#include <sstream>

namespace EVADE {
namespace Fieldspace {

TunnellingDir::TunnellingDir(double bounce, Direction direction)
    : Direction{std::move(direction)}, B_{bounce} {}

TunnellingDir::TunnellingDir(BounceCode bounce, Direction direction)
    : Direction{std::move(direction)}, B_{static_cast<double>(bounce)} {}

double TunnellingDir::B() const { return B_; }

bool operator==(const TunnellingDir &a, const TunnellingDir &b) {
  return ((a.B_ < 0 && b.B_ < 0) || a.B_ == Utilities::Approx(b.B_)
                                                .Abs(TunnellingDir::atolB)
                                                .Rel(TunnellingDir::rtolB)) &&
         (static_cast<Direction>(a) == static_cast<Direction>(b));
}

bool operator<(const TunnellingDir &a, const TunnellingDir &b) {
  if (a == b)
    return false;
  if ((a.B_ < 0 && b.B_ < 0) || a.B_ == Utilities::Approx(b.B_)
                                            .Abs(TunnellingDir::atolB)
                                            .Rel(TunnellingDir::rtolB))
    return static_cast<Direction>(a) < static_cast<Direction>(b);
  if (a.B_ < 0)
    return false;
  if (b.B_ < 0)
    return true;
  return a.B_ < b.B_;
}

std::string TunnellingDir::ToString(IO::PrintInvalidBounces invalidB,
                                    IO::PrintOrigin origin,
                                    IO::PrintLabels labels, int precision,
                                    const std::string &separator) const {
  std::stringstream ss;
  ss.precision(precision);
  if (invalidB == IO::PrintInvalidBounces::yes || B_ >= 0) {
    if (labels == IO::PrintLabels::yes)
      ss << "B: ";
    ss << B_ << separator;
  }
  ss << Direction::ToString(origin, labels, precision, separator);
  return ss.str();
}

std::ostream &operator<<(std::ostream &os, const TunnellingDir &obj) {
  os << obj.ToString();
  return os;
}

} // namespace Fieldspace

} // namespace EVADE
