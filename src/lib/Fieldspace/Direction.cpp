#include "EVADE/Fieldspace/Direction.hpp"

#include "EVADE/Utilities/Approx.hpp"
#include "EVADE/Utilities/Utilities.hpp"

namespace EVADE {
namespace Fieldspace {

Direction::Direction(Point origin, Point target)
    : origin_{std::move(origin)}, target_{std::move(target)},
      dir_{Utilities::NormalizedDirection(origin_.Fields(), target_.Fields())},
      distance_{Utilities::Distance(origin_.Fields(), target_.Fields())} {}

const Point &Direction::Origin() const { return origin_; }
const Point &Direction::Target() const { return target_; }

std::vector<double> Direction::Dir() const { return dir_; }

double Direction::Distance() const { return distance_; }

bool operator==(const Direction &a, const Direction &b) {
  return (a.target_ == b.target_) && (a.origin_ == b.origin_);
}

bool operator<(const Direction &a, const Direction &b) {
  if (a == b)
    return false;
  if (a.origin_ != b.origin_)
    return a.origin_ < b.origin_;
  if (a.target_.Degenerate(b.target_) &&
      a.distance_ !=
          Utilities::Approx(b.distance_).Rel(a.rtolDist).Abs(a.atolDist))
    return a.distance_ < b.distance_;
  return a.target_ < b.target_;
}

std::string Direction::ToString(IO::PrintOrigin origin, IO::PrintLabels labels,
                                int precision,
                                const std::string &separator) const {
  std::stringstream ss;

  if (origin == IO::PrintOrigin::yes) {
    if (labels == IO::PrintLabels::yes)
      ss << "origin: ";
    ss << origin_.ToString(labels, precision, separator) << separator;
  }
  if (labels == IO::PrintLabels::yes)
    ss << "target: ";
  ss << target_.ToString(labels, precision, separator);
  return ss.str();
}
} // namespace Fieldspace
} // namespace EVADE
