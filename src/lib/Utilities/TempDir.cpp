#include "EVADE/Utilities/TempDir.hpp"

#include "EVADE/Utilities/Utilities.hpp"

#include <algorithm>
#include <cstdio>
#include <errno.h>
#include <sys/stat.h>
#include <system_error>
#include <unistd.h>

EVADE::Utilities::TempDir::TempDir(const std::string &prefixPath)
    : path_{RemoveDoubleSlashes(prefixPath)}, filesToClean_{}, dirsToClean_{} {
  path_ += std::to_string(getpid()) + "_";
  path_ += std::to_string(++count_) + "/";
  // create the directory tree
  size_t loc = 0;
  while (loc != std::string::npos) {
    loc = path_.find("/", loc + 1);
    if (mkdir(path_.substr(0, loc).c_str(), S_IRWXU) == 0) {
      dirsToClean_.push_back(path_.substr(0, loc));
    } else if (errno != EEXIST) {
      throw(std::system_error(errno, std::system_category(),
                              "Could not create directory: " +
                                  path_.substr(0, loc)));
    }
  }
}

void EVADE::Utilities::TempDir::MakeSubfolder(const std::string &location) {
  std::string s = RemoveDoubleSlashes(path_ + location);
  if ((mkdir(s.c_str(), S_IRWXU) != 0) && (errno != EEXIST)) {
    throw(std::system_error(errno, std::system_category(),
                            "Could not create directory: " + s));
  }
  dirsToClean_.push_back(s);
}

void EVADE::Utilities::TempDir::MakeSymlink(const std::string &original,
                                            const std::string &location) {
  std::string s = RemoveDoubleSlashes(path_ + location);
  if (symlink(original.c_str(), s.c_str()) != 0) {
    throw(std::system_error(errno, std::system_category(),
                            "Could not create symlink: " + s + " -> " +
                                original));
  }
  filesToClean_.push_back(s);
}

void EVADE::Utilities::TempDir::SetCleanup(
    const std::vector<std::string> &filesToClean) {
  for (const auto &file : filesToClean) {
    filesToClean_.push_back(RemoveDoubleSlashes(path_ + file));
  }
}

void EVADE::Utilities::TempDir::DontClean(const std::string &file) {
  filesToClean_.erase(std::remove(filesToClean_.begin(), filesToClean_.end(),
                                  RemoveDoubleSlashes(path_ + file)),
                      filesToClean_.end());
}

EVADE::Utilities::TempDir::~TempDir() {
  if (!path_.empty()) {
    for (auto rit = filesToClean_.crbegin(); rit != filesToClean_.crend();
         ++rit) {
      std::remove(rit->c_str());
    }
    for (auto rit = dirsToClean_.crbegin(); rit != dirsToClean_.crend();
         ++rit) {
      rmdir(rit->c_str());
    }
  }
}

std::atomic<size_t> EVADE::Utilities::TempDir::count_(0);
