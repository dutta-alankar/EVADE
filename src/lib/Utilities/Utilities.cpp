#include "EVADE/Utilities/Utilities.hpp"

#include "EVADE/Utilities/Approx.hpp"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <numeric>

namespace EVADE {
namespace Utilities {

bool IsReal(const std::complex<double> &x) {
  constexpr double absTol = 2e-7; // adjusted to what HOM4PS2 considers real
  return (Approx(x.imag()).Abs(absTol) == 0) ||
         (Approx(x.real()) != 0 &&
          Approx(x.imag() / x.real()).Abs(absTol) == 0);
}

bool IsRealVector(const std::vector<std::complex<double>> &vec) {
  for (const auto &x : vec) {
    if (!IsReal(x)) {
      return false;
    }
  }
  return true;
}

std::vector<double> RealPart(const std::vector<std::complex<double>> &vec) {
  std::vector<double> result(vec.size());
  for (std::size_t i = 0; i != result.size(); i++) {
    result[i] = vec[i].real();
  }
  return result;
}

double Norm(const std::vector<double> &vec) {
  return std::sqrt(
      std::accumulate(vec.begin(), vec.end(), 0.0,
                      [](double l, double r) { return l + r * r; }));
}

void Normalize(std::vector<double> &vec) {
  const double norm = Norm(vec);

  if (norm > 100 * std::numeric_limits<double>::min()) {
    for (auto &x : vec) {
      x /= norm;
    }
  } else {
    vec = std::vector<double>(vec.size(), 0.);
  }
}

size_t FindStringLocationIn(const std::string &target,
                            const std::vector<std::string> storage) {
  size_t location = static_cast<size_t>(
      std::find(storage.begin(), storage.end(), target) - storage.begin());

  assert(location < storage.size());
  return location;
}

std::string RemoveDoubleSlashes(std::string path) {
  size_t loc = 0;

  while ((loc = path.find("//")) != std::string::npos) {
    path.replace(loc, 2, "/");
  }
  return path;
}

std::vector<size_t>
PositionsOfElementsIn(const std::vector<std::string> &targets,
                      const std::vector<std::string> &source) {
  std::vector<size_t> result;

  for (size_t i = 0; i < source.size(); ++i) {
    if (std::find(targets.begin(), targets.end(), source[i]) != targets.end()) {
      result.push_back(i);
    }
  }
  return result;
}

std::vector<double> NormalizedDirection(const std::vector<double> &from,
                                        const std::vector<double> &to) {
  std::vector<double> direction(from.size());
  std::transform(to.begin(), to.end(), from.begin(), direction.begin(),
                 [](double toE, double fromE) { return toE - fromE; });

  Normalize(direction);
  return direction;
}

double Distance(const std::vector<double> &from,
                const std::vector<double> &to) {
  std::vector<double> direction(from.size());
  std::transform(to.begin(), to.end(), from.begin(), direction.begin(),
                 [](double toE, double fromE) { return toE - fromE; });
  return Norm(direction);
}

std::vector<double> &
ReinsertZeroFields(std::vector<double> &reducedFields,
                   const std::vector<size_t> &nonZeroFields, size_t nFields) {
  std::vector<double> result(nFields, 0);
  for (size_t i = 0; i != reducedFields.size(); ++i) {
    result[nonZeroFields[i]] = reducedFields[i];
  }
  std::swap(result, reducedFields);
  return reducedFields;
}

} // namespace Utilities
} // namespace EVADE
