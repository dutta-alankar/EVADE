#include "EVADE/Utilities/Polynomial.hpp"

#include "EVADE/Utilities/Approx.hpp"

#include <iostream>
#include <memory>
#include <numeric>
#include <sstream>
#include <stdexcept>
#include <utility>

void EVADE::Utilities::Polynomial::SetOtherVariablesToZero(
    const std::vector<size_t> &nonZeroVariables) {
  std::vector<bool> varIsZero(data_.begin()->first.size(), true);
  for (size_t i : nonZeroVariables) {
    varIsZero.at(i) = false;
  }
  decltype(data_) result;
  for (auto it = data_.begin(); it != data_.end(); it++) {
    if (!Detail::TermIsZero(*it, varIsZero)) {
      key_type reduced_key;
      for (size_t i : nonZeroVariables) {
        reduced_key.emplace_back(it->first[i]);
      }
      result[reduced_key] += it->second;
    }
  }
  std::swap(data_, result);
}

bool EVADE::Utilities::Detail::TermIsZero(const Polynomial::value_type &term,
                                          const std::vector<bool> &varIsZero) {
  for (size_t i = 0; i != varIsZero.size(); ++i) {
    if (varIsZero.at(i) && (term.first.at(i) != 0)) {
      return true;
    }
  }
  return false;
}

EVADE::Utilities::Polynomial::value_type
EVADE::Utilities::Detail::DerivateTermBy(const Polynomial::value_type &term,
                                         size_t var) {
  Polynomial::key_type newkey = term.first;
  newkey[var] -= 1;
  return {newkey, term.second * term.first[var]};
}

EVADE::Utilities::Polynomial
EVADE::Utilities::Polynomial::DerivateBy(size_t var) const {
  Polynomial derivative;

  for (auto it = data_.begin(); it != data_.end(); ++it) {
    Polynomial::value_type dTerm = Detail::DerivateTermBy(*it, var);
    if (Approx(dTerm.second) != 0.) {
      derivative[dTerm.first] += dTerm.second;
    }
  }
  return derivative;
}

std::vector<EVADE::Utilities::Polynomial>
EVADE::Utilities::DivergenceForNonZero(
    const Polynomial &poly, const std::vector<size_t> &nonZeroVariables) {
  std::vector<Polynomial> result;
  for (size_t i : nonZeroVariables) {
    result.emplace_back(poly.DerivateBy(i));
    result.back().SetOtherVariablesToZero(nonZeroVariables);
  }
  return result;
}

bool EVADE::Utilities::Polynomial::KeyCompare::
operator()(const std::vector<unsigned> &a,
           const std::vector<unsigned> &b) const {
  if (a.size() != b.size()) {
    throw std::out_of_range(
        "Cannot add Polynomials with different variable counts (" +
        std::to_string(a.size()) + " vs " + std::to_string(b.size()) + ").");
  }

  unsigned dega = std::accumulate(a.begin(), a.end(), static_cast<unsigned>(0));
  unsigned degb = std::accumulate(b.begin(), b.end(), static_cast<unsigned>(0));

  if (dega == degb) {
    return a < b;
  } else {
    return dega < degb;
  }
}

std::string
EVADE::Utilities::Polynomial::ToString(const std::vector<std::string> &varnames,
                                       int precision,
                                       const std::string &exponentiator) const {
  std::ostringstream os;
  os.precision(precision);
  os << std::noshowpos << std::scientific; // avoid leading + signs
  for (const auto &monomial : data_) {     // iterate over individual terms
    if (monomial.second != 0.0) {
      os << monomial.second << std::showpos; // write coefficient
      for (size_t i = 0; i < monomial.first.size(); ++i) {
        if (monomial.first[i] != 0) {
          os << std::noshowpos;
          os << "*" << varnames.at(i) << exponentiator
             << monomial.first[i]; // write variables
          os << std::showpos;
        }
      }
    }
  }
  return os.str();
}

std::ostream &EVADE::Utilities::operator<<(std::ostream &os,
                                           const Polynomial &obj) {
  std::vector<std::string> varnames(obj.VarCount(), "x");
  for (size_t i = 0; i != varnames.size(); i++) {
    varnames[i] += std::to_string(i);
  }
  os << obj.ToString(varnames);
  return os;
}

double &EVADE::Utilities::Polynomial::operator[](const key_type &key) {
  return data_[key];
}

double &EVADE::Utilities::Polynomial::operator[](key_type &&key) {
  return data_[std::move(key)];
}

double &EVADE::Utilities::Polynomial::at(const key_type &key) {
  return data_.at(key);
}

EVADE::Utilities::Polynomial::iterator
EVADE::Utilities::Polynomial::begin() noexcept {
  return data_.begin();
}

EVADE::Utilities::Polynomial::const_iterator
EVADE::Utilities::Polynomial::begin() const noexcept {
  return data_.cbegin();
}

EVADE::Utilities::Polynomial::iterator
EVADE::Utilities::Polynomial::end() noexcept {
  return data_.end();
}

EVADE::Utilities::Polynomial::const_iterator
EVADE::Utilities::Polynomial::end() const noexcept {
  return data_.cend();
}

size_t EVADE::Utilities::Polynomial::size() const noexcept {
  return data_.size();
}

size_t EVADE::Utilities::Polynomial::VarCount() const noexcept {
  if (data_.size() == 0) {
    return 0;
  } else {
    return data_.cbegin()->first.size();
  }
}

EVADE::Utilities::Polynomial EVADE::Utilities::UnitSphere(size_t d) {
  Polynomial unitSphere;

  for (size_t i = 0; i != d; ++i) {
    Polynomial::key_type term(d, 0);
    term[i] = 2;
    unitSphere[term] = 1;
  }
  unitSphere[Utilities::Polynomial::key_type(d, 0)] = -1;
  return unitSphere;
}
